import {List, Map, toJS, fromJS} from 'immutable';
import {expect} from 'chai';

import gameReducer from '../src/reducers/game_reducer.js';
import PHASES from '../src/game_logic/phases.js';

describe('scenario tests -', () => {
    it('the next player gets his turn', () => {
        const state = fromJS({
            map: [
                ['S', 'R', 'D'], 
                ['S', 'V', 'M'],
                ['F', 'P', 'D'],
                ['F', 'P', 'V'],
            ],
            players:
            [
                {
                    name: "Player1",
                    cities: [
                        {x: 0, y: 1},
                        {x: 2, y: 0}
                    ],
                    gold: 4
                },
                {            
                    name: "Player2",
                    cities: [
                        {x: 1, y: 0},
                        {x: 0, y: 0}
                    ],
                    gold: 5   
                }
            ],
            phase: PHASES.RECRUIT,
            currentPlayer: 1,
            secret: {
                deck: [
                    {id: 'Mutiny', lifespan: 5},
                    {id: 'Mercenary', lifespan: 1}
                ]
            }
        });
        
        let nextState = gameReducer(state, {type: 'end_turn', user: "Player2"});
        expect(nextState.get('phase')).to.equal(PHASES.DEAL_CARD);
        expect(nextState.get('currentPlayer')).to.equal(0);        
        expect(nextState.get('players').get(0).get('gold')).to.equal(5);
        expect(nextState.get('players').get(1).get('gold')).to.equal(8);
    });
});
