import {List, Map, toJS, fromJS} from 'immutable';
import {expect} from 'chai';

import {allMercenariesManaged, payMercenary, disbandMercenary} from '../src/game_logic/phases/pay_mercenaries.js';
import PHASES from '../src/game_logic/phases.js';

describe('manage mercenaries -', () => {
  it('can disband mercenary', () => {
    const state = fromJS({
        players:
        [
            { 
                hoplites: [{x: 1, y: 1, isMercenary: true}]
            }
        ],
        phase: PHASES.PAY_MERCENARIES,
        currentPlayer: 0
      });
    expect(allMercenariesManaged(state)).to.be.false
    const nextState = disbandMercenary(state, {hopliteIndex: 0});
    
    expect(nextState.get('players').get(0).get('hoplites').size).to.equal(0);
    expect(allMercenariesManaged(nextState)).to.be.true      
  });

  it('can pay mercenary', () => {
    const state = fromJS({
        players:
        [
            { 
                hoplites: [{x: 1, y: 1, isMercenary: true}],
                gold: 2
            }
        ],
        phase: PHASES.PAY_MERCENARIES,
        currentPlayer: 0
      });
    expect(allMercenariesManaged(state)).to.be.false
    const nextState = payMercenary(state, {hopliteIndex: 0});
    
    expect(nextState.get('players').get(0).get('hoplites').size).to.equal(1);
    expect(nextState.get('players').get(0).get('gold')).to.equal(0);
    expect(allMercenariesManaged(nextState)).to.be.true      
  });

  it('cannot disband regular hoplite ', () => {
    const state = fromJS({
        players:
        [
            { 
                hoplites: [{x: 1, y: 1}],
                gold: 2
            }
        ],
        phase: PHASES.PAY_MERCENARIES,
        currentPlayer: 0
      });
    expect(allMercenariesManaged(state)).to.be.true
    const nextState = disbandMercenary(state, {hopliteIndex: 0});
    
    expect(nextState.get('players').get(0).get('hoplites').size).to.equal(1);
    expect(allMercenariesManaged(nextState)).to.be.true      
  });

  it('cannot pay regular hoplite ', () => {
    const state = fromJS({
        players:
        [
            { 
                hoplites: [{x: 1, y: 1}],
                gold: 2
            }
        ],
        phase: PHASES.PAY_MERCENARIES,
        currentPlayer: 0
      });
    expect(allMercenariesManaged(state)).to.be.true
    const nextState = payMercenary(state, {hopliteIndex: 0});
    
    expect(nextState.get('players').get(0).get('hoplites').size).to.equal(1);
    expect(nextState.get('players').get(0).get('gold')).to.equal(2);
    expect(allMercenariesManaged(nextState)).to.be.true      
  });

  it('cannot pay mercenary with less than 2 gold', () => {
    const state = fromJS({
        players:
        [
            { 
                hoplites: [{x: 1, y: 1, isMercenary: true}],
                gold: 1
            }
        ],
        phase: PHASES.PAY_MERCENARIES,
        currentPlayer: 0
      });
    expect(allMercenariesManaged(state)).to.be.false
    const nextState = payMercenary(state, {hopliteIndex: 0});
    
    expect(nextState.get('players').get(0).get('hoplites').size).to.equal(1);
    expect(nextState.get('players').get(0).get('gold')).to.equal(1);
    expect(allMercenariesManaged(nextState)).to.be.false 
  });
});
