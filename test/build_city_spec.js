import {List, Map, toJS, fromJS} from 'immutable';
import {expect} from 'chai';

import {buildCity} from '../src/game_logic/phases/move_settlers.js';
import PHASES from '../src/game_logic/phases.js';

describe('building cities -', () => {
  it('can build city on terrain hex', () => {
    const state = fromJS({
        map: [
            ['S', 'R'], 
            ['S', 'V']
        ],
        players:
        [
            {
                settlers: [{x: 1, y: 0}],
                cities: [{x: 0, y: 0, hasCaravan: false}],
                food: 6,
            }
        ],
        phase: PHASES.MOVE_SETTLERS,
        currentPlayer: 0
      });
    const nextState = buildCity(state, {settlerIndex: 0});
    
    expect(nextState.get('players').get(0).get('food')).to.equal(5);
    expect(nextState.get('players').get(0).get('cities').get(1)).to.equal(Map({x: 1, y: 0, hasCaravan: false})); 
    expect(nextState.get('players').get(0).get('settlers').size).to.equal(0); 
  });
  
  it('cost of city equals number of cities', () => {
    const state = fromJS({
        map: [
            ['S', 'R'], 
            ['S', 'V']
        ],
        players:
        [
            {
                settlers: [{x: 1, y: 0}, {x: 0, y: 0}],
                cities: [{x: 0, y: 0, hasCaravan: false}, {x: 0, y: 1, hasCaravan: false}],
                food: 6,
            }
        ],
        phase: PHASES.MOVE_SETTLERS,
        currentPlayer: 0
      });
    const nextState = buildCity(state, {settlerIndex: 0});
    
    expect(nextState.get('players').get(0).get('food')).to.equal(4);
    expect(nextState.get('players').get(0).get('cities').get(2)).to.equal(Map({x: 1, y: 0, hasCaravan: false})); 
    expect(nextState.get('players').get(0).get('settlers').get(0)).to.equal(Map({x: 0, y: 0}));
  });
  
  it('cannot build city at neutral village ', () => {
    const state = fromJS({
        map: [
            ['S', 'R'], 
            ['S', 'V']
        ],
        players:
        [
            {
                settlers: [{x: 1, y: 1}],
                cities: [{x: 0, y: 0, hasCaravan: false}],
                food: 6,
            }
        ],
        phase: PHASES.MOVE_SETTLERS,
        currentPlayer: 0
      });
    const nextState = buildCity(state, {settlerIndex: 0});
    
    expect(nextState.get('players').get(0).get('food')).to.equal(6);
    expect(nextState.get('players').get(0).get('cities').size).to.equal(1);
    expect(nextState.get('players').get(0).get('settlers').size).to.equal(1);
  });

  it('cannot build city at same location as own city ', () => {
    const state = fromJS({
        map: [
            ['S', 'R'], 
            ['S', 'V']
        ],
        players:
        [
            {
                settlers: [{x: 1, y: 0}],
                cities: [{x: 1, y: 0, hasCaravan: false}],
                food: 6,
            }
        ],
        phase: PHASES.MOVE_SETTLERS,
        currentPlayer: 0
      });
    const nextState = buildCity(state, {settlerIndex: 0});
    
    expect(nextState.get('players').get(0).get('food')).to.equal(6);
    expect(nextState.get('players').get(0).get('cities').size).to.equal(1);
    expect(nextState.get('players').get(0).get('settlers').size).to.equal(1);
  });
  
    it('cannot build city if not enough food ', () => {
    const state = fromJS({
        map: [
            ['S', 'R'], 
            ['S', 'V']
        ],
        players:
        [
            {
                settlers: [{x: 1, y: 0}],
                cities: [{x: 0, y: 0, hasCaravan: false}],
                food: 0,
            }
        ],
        phase: PHASES.MOVE_SETTLERS,
        currentPlayer: 0
      });
    const nextState = buildCity(state, {settlerIndex: 0});
    
    expect(nextState.get('players').get(0).get('cities').size).to.equal(1);
    expect(nextState.get('players').get(0).get('settlers').size).to.equal(1);
  });

  it('cannot build city with a settler that has already moved ', () => {
    const state = fromJS({
        map: [
            ['S', 'R'], 
            ['S', 'V']
        ],
        players:
        [
            {
                settlers: [{x: 1, y: 0, hasMoved: true}],
                cities: [{x: 0, y: 0, hasCaravan: false}],
                food: 1,
            }
        ],
        phase: PHASES.MOVE_SETTLERS,
        currentPlayer: 0
      });
    const nextState = buildCity(state, {settlerIndex: 0});

    expect(nextState.get('players').get(0).get('food')).to.equal(1);
    expect(nextState.get('players').get(0).get('cities').size).to.equal(1);
    expect(nextState.get('players').get(0).get('settlers').size).to.equal(1);
  });

  it('cannot build city with a bad settler index', () => {
    const state = fromJS({
        map: [
            ['S', 'R'], 
            ['S', 'V']
        ],
        players:
        [
            {
                settlers: [{x: 1, y: 0}],
                cities: [{x: 0, y: 0, hasCaravan: false}],
                food: 1,
            }
        ],
        phase: PHASES.MOVE_SETTLERS,
        currentPlayer: 0
      });
    const nextState = buildCity(state, {settlerIndex: -1});

    expect(nextState.get('players').get(0).get('food')).to.equal(1);
    expect(nextState.get('players').get(0).get('cities').size).to.equal(1);
    expect(nextState.get('players').get(0).get('settlers').size).to.equal(1);
  });
});
