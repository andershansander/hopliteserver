import {List, Map, toJS, fromJS} from 'immutable';
import {expect} from 'chai';

import {collectIncome} from '../src/game_logic/phases/income.js';
import PHASES from '../src/game_logic/phases.js';

describe('income phase -', () => {
    describe('river city -', () => {
        it('players with river tile cities get 2 gold', () => {
            const state = fromJS({
                map: [
                    ['S', 'R', 'D'], 
                    ['S', 'V', 'M'],
                    ['F', 'P', 'D'],
                    ['F', 'P', 'V'],
                ],
                players:
                [
                    {
                        cities: [
                            {x: 1, y: 0}
                        ]
                    },
                    {               
                        cities: [
                            {x: 2, y: 0}
                        ]         
                    }
                ],
                phase: PHASES.INCOME,
                currentPlayer: 0
            });
            
            let nextState = collectIncome(state);
            nextState = nextState.set('currentPlayer', 1);
            nextState = collectIncome(nextState);
            expect(nextState.get('players').get(0).get('gold')).to.equal(2);
        });

        it('second player with river tile city and start city get 3 gold', () => {
            const state = fromJS({
                map: [
                    ['S', 'R', 'D'], 
                    ['S', 'V', 'M'],
                    ['F', 'P', 'D'],
                    ['F', 'P', 'V'],
                ],
                players:
                [
                    {
                        cities: [
                            {x: 0, y: 1},
                            {x: 2, y: 0}
                        ],
                        gold: 4
                    },
                    {               
                        cities: [
                            {x: 1, y: 0},
                            {x: 0, y: 0}
                        ],
                        gold: 5   
                    }
                ],
                phase: PHASES.INCOME,
                currentPlayer: 0
            });
            
            let nextState = collectIncome(state);
            nextState = nextState.set('currentPlayer', 1);
            nextState = collectIncome(nextState);
            expect(nextState.get('players').get(0).get('gold')).to.equal(5);
            expect(nextState.get('players').get(1).get('gold')).to.equal(8);
        });

        it('player without river tile do not get 2 gold', () => {
            const state = fromJS({
                map: [
                    ['S', 'R', 'D'], 
                    ['S', 'V', 'M'],
                    ['F', 'P', 'D'],
                    ['F', 'P', 'V'],
                ],
                players:
                [
                    {
                        cities: [
                            {x: 1, y: 0}
                        ]
                    },
                    {               
                        cities: [
                            {x: 2, y: 0}
                        ]         
                    }
                ],
                phase: PHASES.INCOME,
                currentPlayer: 1
            });
            
            const nextState = collectIncome(state);
            expect(nextState.get('players').get(1).get('gold')).to.equal(0);
        });
    });

    describe('plain city -', () => {
        it('players with plain tile cities get 1 food', () => {
            const state = fromJS({
                map: [
                    ['S', 'R', 'D'], 
                    ['S', 'V', 'M'],
                    ['F', 'P', 'D'],
                    ['F', 'P', 'V'],
                ],
                players:
                [
                    {
                        cities: [
                            {x: 1, y: 2}
                        ]
                    },
                    {               
                        cities: [
                            {x: 2, y: 0}
                        ]         
                    }
                ],
                phase: PHASES.INCOME,
                currentPlayer: 0
            });
            
            const nextState = collectIncome(state);
            expect(nextState.get('players').get(0).get('food')).to.equal(1);
        });

        it('player without plain tile do not get 1 food', () => {
            const state = fromJS({
                map: [
                    ['S', 'R', 'D'], 
                    ['S', 'V', 'M'],
                    ['F', 'P', 'D'],
                    ['F', 'P', 'V'],
                ],
                players:
                [
                    {
                        cities: [
                            {x: 1, y: 2}
                        ]
                    },
                    {               
                        cities: [
                            {x: 2, y: 0}
                        ]         
                    }
                ],
                phase: PHASES.INCOME,
                currentPlayer: 1
            });
            
            const nextState = collectIncome(state);
            expect(nextState.get('players').get(1).get('food')).to.equal(0);
        });
    });

    describe('start city -', () => {
        it('players\' start cities get 1 food and 1 gold', () => {
            const state = fromJS({
                map: [
                    ['S', 'R', 'D'], 
                    ['S', 'V', 'M'],
                    ['F', 'P', 'D'],
                    ['F', 'P', 'V'],
                ],
                players:
                [
                    {
                        cities: [
                            {x: 0, y: 0}
                        ]
                    },
                    {               
                        cities: [
                            {x: 0, y: 1}
                        ]         
                    }
                ],
                phase: PHASES.INCOME,
                currentPlayer: 0
            });
            
            const nextState = collectIncome(state);
            expect(nextState.get('players').get(0).get('food')).to.equal(1);
            expect(nextState.get('players').get(0).get('gold')).to.equal(1);
        });

        it('player without start city do not get 1 food or gold', () => {
            const state = fromJS({
                map: [
                    ['S', 'R', 'D'], 
                    ['S', 'V', 'M'],
                    ['F', 'P', 'D'],
                    ['F', 'P', 'V'],
                ],
                players:
                [
                    {
                        cities: [
                            {x: 0, y: 0}
                        ]
                    },
                    {               
                        cities: [
                            {x: 2, y: 0}
                        ]         
                    }
                ],
                phase: PHASES.INCOME,
                currentPlayer: 1
            });
            
            const nextState = collectIncome(state);
            expect(nextState.get('players').get(1).get('food')).to.equal(0);
            expect(nextState.get('players').get(1).get('gold')).to.equal(0);
        });
    });

    describe('desert city -', () => {
        it('players with desert tile cities get 0 gold and 0 food', () => {
            const state = fromJS({
                map: [
                    ['S', 'R', 'D'], 
                    ['S', 'V', 'M'],
                    ['F', 'P', 'D'],
                    ['F', 'P', 'V'],
                ],
                players:
                [
                    {
                        cities: [
                            {x: 2, y: 0}
                        ]
                    },
                    {               
                        cities: [
                            {x: 0, y: 0}
                        ]         
                    }
                ],
                phase: PHASES.INCOME,
                currentPlayer: 0
            });
            
            const nextState = collectIncome(state);
            expect(nextState.get('players').get(0).get('gold')).to.equal(0);
            expect(nextState.get('players').get(0).get('food')).to.equal(0);
        });
    });

    describe('mountain city -', () => {
        it('players with mountain tile cities get no food and no gold', () => {
            const state = fromJS({
                map: [
                    ['S', 'R', 'D'], 
                    ['S', 'V', 'M'],
                    ['F', 'P', 'D'],
                    ['F', 'P', 'V'],
                ],
                players:
                [
                    {
                        cities: [
                            {x: 2, y: 1}
                        ]
                    },
                    {               
                        cities: [
                            {x: 2, y: 0}
                        ]         
                    }
                ],
                phase: PHASES.INCOME,
                currentPlayer: 0
            });
            
            const nextState = collectIncome(state);
            expect(nextState.get('players').get(0).get('gold')).to.equal(0);
            expect(nextState.get('players').get(0).get('food')).to.equal(0);
        });

        it('players with mountain tile cities get 2 additional industry', () => {
            const state = fromJS({
                map: [
                    ['S', 'R', 'D'], 
                    ['S', 'V', 'M'],
                    ['F', 'P', 'D'],
                    ['F', 'P', 'V'],
                ],
                players:
                [
                    {
                        cities: [
                            {x: 2, y: 1}
                        ]
                    },
                    {               
                        cities: [
                            {x: 2, y: 0}
                        ]         
                    }
                ],
                phase: PHASES.INCOME,
                currentPlayer: 0
            });
            
            const nextState = collectIncome(state);
            expect(nextState.get('players').get(0).get('industry')).to.equal(2);
        });

        it('players without mountain tile cities do not get 2 industry', () => {
            const state = fromJS({
                map: [
                    ['S', 'R', 'D'], 
                    ['S', 'V', 'M'],
                    ['F', 'P', 'D'],
                    ['F', 'P', 'V'],
                ],
                players:
                [
                    {
                        cities: [
                            {x: 2, y: 1}
                        ]
                    },
                    {               
                        cities: [
                            {x: 2, y: 0}
                        ]         
                    }
                ],
                phase: PHASES.INCOME,
                currentPlayer: 1
            });
            
            const nextState = collectIncome(state);
            expect(nextState.get('players').get(1).get('industry')).to.equal(0);
        });
    });

    describe('forrest city -', () => {
        it('players with forrest tile cities get no food and no gold', () => {
            const state = fromJS({
                map: [
                    ['S', 'R', 'D'], 
                    ['S', 'V', 'M'],
                    ['F', 'P', 'D'],
                    ['F', 'P', 'V'],
                ],
                players:
                [
                    {
                        cities: [
                            {x: 0, y: 2}
                        ]
                    },
                    {               
                        cities: [
                            {x: 2, y: 0}
                        ]         
                    }
                ],
                phase: PHASES.INCOME,
                currentPlayer: 0
            });
            
            const nextState = collectIncome(state);
            expect(nextState.get('players').get(0).get('gold')).to.equal(0);
            expect(nextState.get('players').get(0).get('food')).to.equal(0);
        });

        it('players with forrest tile cities get 3 additional industry', () => {
            const state = fromJS({
                map: [
                    ['S', 'R', 'D'], 
                    ['S', 'V', 'M'],
                    ['F', 'P', 'D'],
                    ['F', 'P', 'V'],
                ],
                players:
                [
                    {
                        cities: [
                            {x: 0, y: 2}
                        ]
                    },
                    {               
                        cities: [
                            {x: 2, y: 0}
                        ]         
                    }
                ],
                phase: PHASES.INCOME,
                currentPlayer: 0
            });
            
            const nextState = collectIncome(state);
            expect(nextState.get('players').get(0).get('industry')).to.equal(3);
        });

        it('players without forrest tile cities do not get 3 industry', () => {
            const state = fromJS({
                map: [
                    ['S', 'R', 'D'], 
                    ['S', 'V', 'M'],
                    ['F', 'P', 'D'],
                    ['F', 'P', 'V'],
                ],
                players:
                [
                    {
                        cities: [
                            {x: 2, y: 1}
                        ]
                    },
                    {               
                        cities: [
                            {x: 2, y: 0}
                        ]         
                    }
                ],
                phase: PHASES.INCOME,
                currentPlayer: 1
            });
            
            const nextState = collectIncome(state);
            expect(nextState.get('players').get(1).get('industry')).to.equal(0);
        });
    });

    describe('industry -', () => {
        it('industry stacks', () => {
            const state = fromJS({
                map: [
                    ['S', 'R', 'D'], 
                    ['S', 'V', 'M'],
                    ['F', 'P', 'D'],
                    ['F', 'P', 'V'],
                ],
                players:
                [
                    {
                        cities: [
                            {x: 0, y: 2},
                            {x: 2, y: 1},
                        ]
                    },
                    {               
                        cities: [
                            {x: 2, y: 0}
                        ]         
                    }
                ],
                phase: PHASES.INCOME,
                currentPlayer: 0
            });
            
            const nextState = collectIncome(state);
            expect(nextState.get('players').get(0).get('industry')).to.equal(5);
        });
    });
});
