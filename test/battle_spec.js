import {List, Map, toJS, fromJS} from 'immutable';
import {expect} from 'chai';

import 'babel-polyfill';
import {performBattles} from '../src/game_logic/phases/move_hoplites.js';
import PHASES from '../src/game_logic/phases.js';

describe('battles -', () => {
    describe('field battles -', () => {
        it('winner lost hoplites are marked for removal', () => {
            const state = fromJS({
                map: [
                    ['S', 'R'], 
                    ['S', 'V']
                ],
                players:
                [
                    {
                        hoplites: [
                            {x: 1, y: 0},
                            {x: 1, y: 0},
                            {x: 1, y: 0},
                        ]
                    },
                    {
                        hoplites: [
                            {x: 1, y: 0},
                            {x: 1, y: 0}
                        ]
                    }
                ],
                phase: PHASES.MOVE_HOPLITES_1,
                currentPlayer: 0
            });
            const nextState = performBattles(state);
            expect(nextState.get('players').get(0).get('hoplitesToRemove')).to.equal(fromJS(
                [
                    {
                        location: {x: 1, y: 0},
                        amount: 2
                    }
                ]));            
            expect(nextState.get('players').get(1).get('hoplites')).to.equal(fromJS([]));
            
        });

        it('multiple conflicts are solved', () => {
            const state = fromJS({
                map: [
                    ['S', 'R'], 
                    ['S', 'V']
                ],
                players:
                [
                    {
                        hoplites: [
                            {x: 1, y: 0},
                            {x: 1, y: 0},
                            {x: 1, y: 1},
                        ]
                    },
                    {
                        hoplites: [
                            {x: 1, y: 0},
                            {x: 1, y: 1}
                        ]
                    }
                ],
                phase: PHASES.MOVE_HOPLITES_1,
                currentPlayer: 0
            });
            const nextState = performBattles(state);            
            expect(nextState.get('players').get(0).get('hoplitesToRemove')).to.equal(fromJS(
                [
                    {
                        location: {x: 1, y: 0},
                        amount: 1
                    }
                ]));
            expect(nextState.get('players').get(0).get('hoplites')).to.equal(fromJS([{x: 1, y: 0}, {x: 1, y: 0}]));
            expect(nextState.get('players').get(1).get('hoplites')).to.equal(fromJS([]));            
        });

        it('settler of losing side is removed after conflicts is solved', () => {
            const state = fromJS({
                map: [
                    ['S', 'R'], 
                    ['S', 'V']
                ],
                players:
                [
                    {
                        hoplites: [
                            {x: 1, y: 0},
                            {x: 1, y: 0}
                        ]
                    },
                    {
                        hoplites: [
                            {x: 1, y: 0}
                        ],
                        settlers: [
                            {x: 1, y: 0}
                        ]
                    }
                ],
                phase: PHASES.MOVE_HOPLITES_1,
                currentPlayer: 0
            });
            const nextState = performBattles(state);
            expect(nextState.get('players').get(1).get('settlers')).to.equal(fromJS([]));            
        });

        it('settlers are removed by hoplites', () => {
            const state = fromJS({
                map: [
                    ['S', 'R'], 
                    ['S', 'V']
                ],
                players:
                [
                    {
                        hoplites: [
                            {x: 1, y: 0}
                        ]
                    },
                    {
                        settlers: [
                            {x: 1, y: 0}
                        ],
                        cities: [
                            {x: 2, y: 1}
                        ]
                    }
                ],
                phase: PHASES.MOVE_HOPLITES_1,
                currentPlayer: 0
            });
            const nextState = performBattles(state);
            expect(nextState.get('players').get(0).get('hoplites')).to.equal(fromJS([ {x: 1, y: 0}]));  
            expect(nextState.get('players').get(1).get('settlers')).to.equal(fromJS([]));            
        });

        it('settler of losing side is not removed after conflict in other square', () => {
            const state = fromJS({
                map: [
                    ['S', 'R'], 
                    ['S', 'V']
                ],
                players:
                [
                    {
                        hoplites: [
                            {x: 1, y: 0},
                            {x: 1, y: 0}
                        ]
                    },
                    {
                        hoplites: [
                            {x: 1, y: 0}
                        ],
                        settlers: [
                            {x: 0, y: 0}
                        ]
                    }
                ],
                phase: PHASES.MOVE_HOPLITES_1,
                currentPlayer: 0
            });
            const nextState = performBattles(state);
            expect(nextState.get('players').get(1).get('settlers')).to.equal(fromJS([{x: 0, y: 0}]));
        });

        it('conflicts do not arise when hoplites not on same square', () => {
            const state = fromJS({
                map: [
                    ['S', 'R'], 
                    ['S', 'V']
                ],
                players:
                [
                    {
                        hoplites: [
                            {x: 0, y: 0},
                            {x: 1, y: 0},
                            {x: 0, y: 0},
                        ]
                    },
                    {
                        hoplites: [
                            {x: 0, y: 1},
                            {x: 1, y: 1}
                        ]
                    }
                ],
                phase: PHASES.MOVE_HOPLITES_1,
                currentPlayer: 0
            });
            const nextState = performBattles(state);            
            expect(nextState.get('players').get(0).get('hoplitesToRemove')).to.be.undefined;
            expect(nextState.get('players').get(0).get('hoplites')).to.equal(fromJS([
                            {x: 0, y: 0},
                            {x: 1, y: 0},
                            {x: 0, y: 0},
            ]));
            expect(nextState.get('players').get(1).get('hoplites')).to.equal(fromJS([
                            {x: 0, y: 1},
                            {x: 1, y: 1}
            ]));            
        });
    });

    describe('city battles - ', () => {
        it('city and caravans remain when invaders lose', () => {
            const state = fromJS({
                map: [
                    ['S', 'R'], 
                    ['S', 'V']
                ],
                players:
                [
                    {
                        cities: [
                            {x: 0, y: 0}
                        ],
                        hoplites: [
                            {x: 0, y: 1},
                            {x: 0, y: 1},
                            {x: 0, y: 1},
                        ]
                    },
                    {
                        cities: [
                            {x: 0, y: 1, hasCaravan: true},
                        ],
                        hoplites: [
                            {x: 0, y: 1}
                        ]
                    }
                ],
                phase: PHASES.MOVE_HOPLITES_1,
                currentPlayer: 0
            });
            const nextState = performBattles(state);            
            expect(nextState.get('players').get(0).get('hoplites')).to.equal(List());
            expect(nextState.get('players').get(1).get('hoplites')).to.equal(List());
            expect(nextState.get('players').get(0).get('cities')).to.equal(fromJS([{x: 0, y: 0}]));
            expect(nextState.get('players').get(1).get('cities')).to.equal(fromJS([{x: 0, y: 1, hasCaravan: true}]));           
        });

        it('defending hoplites are protected by the city', () => {
            const state = fromJS({
                map: [
                    ['S', 'R'], 
                    ['S', 'V']
                ],
                players:
                [
                    {
                        cities: [
                            {x: 0, y: 0}
                        ],
                        hoplites: [
                            {x: 0, y: 1},
                            {x: 0, y: 1}
                        ]
                    },
                    {
                        cities: [
                            {x: 0, y: 1, hasCaravan: true},
                        ],
                        hoplites: [
                            {x: 0, y: 1}
                        ]
                    }
                ],
                phase: PHASES.MOVE_HOPLITES_1,
                currentPlayer: 0
            });
            const nextState = performBattles(state);            
            expect(nextState.get('players').get(0).get('hoplites')).to.equal(List());
            expect(nextState.get('players').get(1).get('hoplites')).to.equal(fromJS([{x: 0, y: 1}]));
            expect(nextState.get('players').get(1).get('hoplitesToRemove')).to.be.undefined;
            expect(nextState.get('players').get(0).get('cities')).to.equal(fromJS([{x: 0, y: 0}]));
            expect(nextState.get('players').get(1).get('cities')).to.equal(fromJS([{x: 0, y: 1, hasCaravan: true}]));           
        });

        it('cities are captures and caravans removed when invaders win', () => {
            const state = fromJS({
                map: [
                    ['S', 'R'], 
                    ['S', 'V']
                ],
                players:
                [
                    {
                        cities: [
                            {x: 0, y: 0}
                        ],
                        hoplites: [
                            {x: 0, y: 1},
                            {x: 0, y: 1},
                            {x: 0, y: 1},
                            {x: 0, y: 1},
                        ]
                    },
                    {
                        cities: [
                            {x: 0, y: 1, hasCaravan: true},
                            {x: 1, y: 1}
                        ],
                        hoplites: [
                            {x: 0, y: 1},
                            {x: 1, y: 0}
                        ]
                    }
                ],
                phase: PHASES.MOVE_HOPLITES_1,
                currentPlayer: 0
            });
            const nextState = performBattles(state);            
            expect(nextState.get('players').get(0).get('hoplites')).to.equal(fromJS([{x: 0, y: 1}, {x: 0, y: 1}, {x: 0, y: 1}, {x: 0, y: 1}]));
            expect(nextState.get('players').get(0).get('hoplitesToRemove')).to.equal(fromJS([{location: {x: 0, y: 1}, amount: 3}]));
            expect(nextState.get('players').get(1).get('hoplites')).to.equal(fromJS([{x: 1, y: 0}]));
            expect(nextState.get('players').get(0).get('cities')).to.equal(fromJS([{x: 0, y: 0}, {x: 0, y: 1}]));
            expect(nextState.get('players').get(1).get('cities')).to.equal(fromJS([{x: 1, y: 1}]));
        });

        it('cities may be captured while undefended', () => {
            const state = fromJS({
                map: [
                    ['S', 'R'], 
                    ['S', 'V']
                ],
                players:
                [
                    {
                        cities: [
                            {x: 0, y: 0}
                        ],
                        hoplites: [
                            {x: 0, y: 1},
                            {x: 0, y: 1},
                            {x: 0, y: 1},
                        ]
                    },
                    {
                        settlers: [{x: 0, y: 1}],
                        cities: [
                            {x: 0, y: 1, hasCaravan: true},
                            {x: 1, y: 1}
                        ]
                    }
                ],
                phase: PHASES.MOVE_HOPLITES_1,
                currentPlayer: 0
            });
            const nextState = performBattles(state);            
            expect(nextState.get('players').get(0).get('hoplites')).to.equal(fromJS([{x: 0, y: 1}, {x: 0, y: 1}, {x: 0, y: 1}]));
            expect(nextState.get('players').get(0).get('hoplitesToRemove')).to.equal(fromJS([{location: {x: 0, y: 1}, amount: 2}]));
            expect(nextState.get('players').get(0).get('cities')).to.equal(fromJS([{x: 0, y: 0}, {x: 0, y: 1}]));
            expect(nextState.get('players').get(1).get('cities')).to.equal(fromJS([{x: 1, y: 1}]));
            expect(nextState.get('players').get(1).get('settlers')).to.equal(fromJS([]));
        });
    });
});
