import {List, Map, toJS, fromJS} from 'immutable';
import {expect} from 'chai';

import {recruitHoplite} from '../src/game_logic/phases/recruit.js';
import PHASES from '../src/game_logic/phases.js';

describe('recruiting hoplite -', () => {
  it('can recruit hoplite in a friendly town', () => {
    const state = fromJS({
        players:
        [
            { 
                gold: 3,
                cities: [{x: 0, y: 0, hasCaravan: false}],
                food: 6,
            },            
            {
                gold: 0,
                cities: [{x: 0, y: 1, hasCaravan: false}],
                food: 0
            }
        ],
        phase: PHASES.RECRUIT,
        currentPlayer: 0
      });
    const nextState = recruitHoplite(state, {target: {x: 0, y: 0}, cost: {food: 1, gold: 2}});
    
    expect(nextState.get('players').get(0).get('gold')).to.equal(1);
    expect(nextState.get('players').get(0).get('food')).to.equal(5);      
    expect(nextState.get('players').get(0).get('hoplites').get(0)).to.equal(Map({x: 0, y: 0}));  
  });
  
  it('cannot recruit hoplite in a enemy town', () => {
    const state = fromJS({
        players:
        [
            { 
                gold: 3,
                cities: [{x: 0, y: 0, hasCaravan: false}],
                food: 6,
            },            
            {
                gold: 0,
                cities: [{x: 0, y: 1, hasCaravan: false}],
                food: 0
            }
        ],
        phase: PHASES.RECRUIT,
        currentPlayer: 0
      });
    const nextState = recruitHoplite(state, {target: {x: 0, y: 1}, cost: {food: 1, gold: 2}});
    
    expect(nextState.get('players').get(0).get('gold')).to.equal(3);
    expect(nextState.get('players').get(0).get('food')).to.equal(6);      
    expect(nextState.get('players').get(0).get('hoplites').size).to.equal(0);
  });
  
  it('cannot recruit hoplite when short of food', () => {
    const state = fromJS({
        players:
        [
            { 
                gold: 3,
                cities: [{x: 0, y: 0, hasCaravan: false}],
                food: 0,
            },            
            {
                gold: 0,
                cities: [{x: 0, y: 1, hasCaravan: false}],
                food: 0
            }
        ],
        phase: PHASES.RECRUIT,
        currentPlayer: 0
      });
    const nextState = recruitHoplite(state, {target: {x: 0, y: 0}, cost: {food: 1, gold: 2}});
    
    expect(nextState.get('players').get(0).get('gold')).to.equal(3);
    expect(nextState.get('players').get(0).get('food')).to.equal(0);      
    expect(nextState.get('players').get(0).get('hoplites').size).to.equal(0);  
  });
  
  it('cannot recruit hoplite when short of gold', () => {
    const state = fromJS({
        players:
        [
            { 
                gold: 1,
                cities: [{x: 0, y: 0, hasCaravan: false}],
                food: 6,
            },            
            {
                gold: 0,
                cities: [{x: 0, y: 1, hasCaravan: false}],
                food: 0
            }
        ],
        phase: PHASES.RECRUIT,
        currentPlayer: 0
      });
    const nextState = recruitHoplite(state, {target: {x: 0, y: 0}, cost: {food: 1, gold: 2}});
    
    expect(nextState.get('players').get(0).get('gold')).to.equal(1);
    expect(nextState.get('players').get(0).get('food')).to.equal(6);      
    expect(nextState.get('players').get(0).get('hoplites').size).to.equal(0);  
  });

  it('cannot recruit hoplite when no industry', () => {
    const state = fromJS({
        players:
        [
            { 
                gold: 2,
                cities: [{x: 0, y: 0, hasCaravan: false}],
                food: 6,
                industry: 0
            },            
            {
                gold: 0,
                cities: [{x: 0, y: 1, hasCaravan: false}],
                food: 0
            }
        ],
        phase: PHASES.RECRUIT,
        currentPlayer: 0
      });
    const nextState = recruitHoplite(state, {target: {x: 0, y: 0}, cost: {food: 1, gold: 2, industry: 1}});
    
    expect(nextState.get('players').get(0).get('gold')).to.equal(2);
    expect(nextState.get('players').get(0).get('food')).to.equal(6);      
    expect(nextState.get('players').get(0).get('hoplites').size).to.equal(0);  
  });

  it('cannot recruit hoplite when short of industry', () => {
    const state = fromJS({
        players:
        [
            { 
                gold: 2,
                cities: [{x: 0, y: 0, hasCaravan: false}],
                hoplites: [{x: 0, y: 0}, {x: 0, y: 0}],
                food: 6,
                industry: 2
            },            
            {
                gold: 0,
                cities: [{x: 0, y: 1, hasCaravan: false}],
                food: 0
            }
        ],
        phase: PHASES.RECRUIT,
        currentPlayer: 0
      });
    const nextState = recruitHoplite(state, {target: {x: 0, y: 0}, cost: {food: 1, gold: 2, industry: 1}});
    
    expect(nextState.get('players').get(0).get('gold')).to.equal(2);
    expect(nextState.get('players').get(0).get('food')).to.equal(6);
    expect(nextState.get('players').get(0).get('hoplites').size).to.equal(2);  
  });

  it('mercenaries do not count when calculating industry', () => {
    const state = fromJS({
        players:
        [
            { 
                gold: 2,
                cities: [{x: 0, y: 0, hasCaravan: false}],
                hoplites: [{x: 0, y: 0, isMercenary: true}, {x: 0, y: 0}],
                food: 6,
                industry: 2
            },            
            {
                gold: 0,
                cities: [{x: 0, y: 1, hasCaravan: false}],
                food: 0
            }
        ],
        phase: PHASES.RECRUIT,
        currentPlayer: 0
      });
    const nextState = recruitHoplite(state, {target: {x: 0, y: 0}, cost: {food: 1, gold: 2, industry: 1}});
    
    expect(nextState.get('players').get(0).get('gold')).to.equal(0);
    expect(nextState.get('players').get(0).get('food')).to.equal(5);
    expect(nextState.get('players').get(0).get('hoplites').size).to.equal(3);  
  });
});
