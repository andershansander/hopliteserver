import {List, Map, toJS, fromJS} from 'immutable';
import {expect} from 'chai';

import {recruitSettler} from '../src/game_logic/phases/recruit.js';
import PHASES from '../src/game_logic/phases.js';

describe('recruiting -', () => {
  it('can recruit settler in a friendly town', () => {
    const state = fromJS({
        players:
        [
            { 
                gold: 3,
                cities: [{x: 0, y: 0, hasCaravan: false}],
                food: 6,
            },            
            {
                gold: 0,
                cities: [{x: 0, y: 1, hasCaravan: false}],
                food: 0
            }
        ],
        phase: PHASES.RECRUIT,
        currentPlayer: 0
      });
    const nextState = recruitSettler(state, {target: {x: 0, y: 0}, cost: {food: 4, gold: 2}});
    
    expect(nextState.get('players').get(0).get('gold')).to.equal(1);
    expect(nextState.get('players').get(0).get('food')).to.equal(2);      
    expect(nextState.get('players').get(0).get('settlers').get(0)).to.equal(Map({x: 0, y: 0}));  
  });
  
  it('cannot recruit settler in a enemy town', () => {
    const state = fromJS({
        players:
        [
            { 
                gold: 3,
                cities: [{x: 0, y: 0, hasCaravan: false}],
                food: 6,
            },            
            {
                gold: 0,
                cities: [{x: 0, y: 1, hasCaravan: false}],
                food: 0
            }
        ],
        phase: PHASES.RECRUIT,
        currentPlayer: 0
      });
    const nextState = recruitSettler(state, {target: {x: 0, y: 1}, cost: {food: 4, gold: 2}});
    
    expect(nextState.get('players').get(0).get('gold')).to.equal(3);
    expect(nextState.get('players').get(0).get('food')).to.equal(6);      
    expect(nextState.get('players').get(0).get('settlers').size).to.equal(0);  
  });
  
  it('cannot recruit settler when short of food', () => {
    const state = fromJS({
        players:
        [
            { 
                gold: 3,
                cities: [{x: 0, y: 0, hasCaravan: false}],
                food: 3,
            },            
            {
                gold: 0,
                cities: [{x: 0, y: 1, hasCaravan: false}],
                food: 0
            }
        ],
        phase: PHASES.RECRUIT,
        currentPlayer: 0
      });
    const nextState = recruitSettler(state, {target: {x: 0, y: 0}, cost: {food: 4, gold: 2}});
    
    expect(nextState.get('players').get(0).get('gold')).to.equal(3);
    expect(nextState.get('players').get(0).get('food')).to.equal(3);      
    expect(nextState.get('players').get(0).get('settlers').size).to.equal(0);  
  });
  
  it('cannot recruit settler when short of gold', () => {
    const state = fromJS({
        players:
        [
            { 
                gold: 1,
                cities: [{x: 0, y: 0, hasCaravan: false}],
                food: 6,
            },            
            {
                gold: 0,
                cities: [{x: 0, y: 1, hasCaravan: false}],
                food: 0
            }
        ],
        phase: PHASES.RECRUIT,
        currentPlayer: 0
      });
    const nextState = recruitSettler(state, {target: {x: 0, y: 0}, cost: {food: 4, gold: 2}});
    
    expect(nextState.get('players').get(0).get('gold')).to.equal(1);
    expect(nextState.get('players').get(0).get('food')).to.equal(6);      
    expect(nextState.get('players').get(0).get('settlers').size).to.equal(0);  
  });
});
