import {List, Map, toJS, fromJS} from 'immutable';
import {expect} from 'chai';
import PHASES from '../src/game_logic/phases.js';
import gameReducer from '../src/reducers/game_reducer.js';

describe('phase logic -', () => {
    describe('move settler phase -', () => {
        it('the next player gets his turn', () => {
            const state = fromJS({
                players:
                [
                    {
                        name: "player1",
                        settlers: []
                    },
                    {               
                        name: "player2",
                        settlers: [{x: 1, y: 1}]         
                    }
                ],
                phase: PHASES.MOVE_SETTLERS,
                currentPlayer: 0
            });
            
            const nextState = gameReducer(state, {type: 'end_turn', user: "player1"});
            expect(nextState.get('phase')).to.equal(PHASES.MOVE_SETTLERS);
            expect(nextState.get('currentPlayer')).to.equal(1);
        });
        
        it('the first move hoplite phase is entered', () => {
            const state = fromJS({
                players:
                [
                    {
                        name: "player1",
                        settlers: [],
                        hoplites: [{x: 1, y: 1}]
                    },
                    {              
                        name: "player2", 
                        settlers: []         
                    }
                ],
                phase: PHASES.MOVE_SETTLERS,
                currentPlayer: 1
            });
            
            const nextState = gameReducer(state, {type: 'end_turn', user: "player2"});
            expect(nextState.get('phase')).to.equal(PHASES.MOVE_HOPLITES_1);
            expect(nextState.get('currentPlayer')).to.equal(0);
        });
        
        it('the players settlers get their moved flag removed', () => {
            const state = fromJS({
                players:
                [
                    {
                        name: "player1",
                        settlers: [{x: 1, y: 0, hasMoved: true}]
                    }
                ],
                phase: PHASES.MOVE_SETTLERS,
                currentPlayer: 0
            });
            const nextState = gameReducer(state, {type: 'end_turn', user: "player1"});
            expect(nextState.get('players').get(0).get('settlers').get(0)).to.equal(Map({x: 1, y: 0})); 
        });

        it('if the player does not have any settlers, the move settler phase is skipped', () => {
            const state = fromJS({
                players:
                [
                    {
                        name: "player1",
                        hoplites: [{x: 1, y: 0}]
                    }
                ],
                phase: PHASES.PAY_MERCENARIES,
                currentPlayer: 0
            });
            const nextState = gameReducer(state, {type: 'end_turn', user: "player1"});
            expect(nextState.get('phase')).to.equal(PHASES.MOVE_HOPLITES_1); 
        });
    });
    describe('move hoplite phase -', () => {
        it('the players hoplites get their moved flag removed', () => {
            const state = fromJS({
                players:
                [
                    {
                        name: "player1",
                        hoplites: [{x: 1, y: 0, hasMoved: true}]
                    }
                ],
                phase: PHASES.MOVE_HOPLITES_1,
                currentPlayer: 0
            });
            const nextState = gameReducer(state, {type: 'end_turn', user: "player1"});
            expect(nextState.get('players').get(0).get('hoplites').get(0)).to.equal(Map({x: 1, y: 0})); 
        });

        it('when there is no conflicts to resolve no user action is required to pass those phases', () => {
            const state = fromJS({
                players:
                [
                    {
                        name: "player1",
                        hoplites: [{x: 1, y: 0, hasMoved: true}, {x: 2, y: 1}]
                    },
                    {
                        name: "player2",
                        hoplites: [{x: 1, y: 0, hasMoved: true}]
                    }
                ],
                phase: PHASES.MOVE_HOPLITES_1,
                currentPlayer: 1
            });
            const nextState = gameReducer(state, {type: 'end_turn', user: "player2"});
            expect(nextState.get('phase')).to.equal(PHASES.MOVE_HOPLITES_2); 
        });

        it('when there is no conflicts to resolve no user action is required to pass conflict phase number 2', () => {
            const state = fromJS({
                players:
                [
                    {
                        name: "player1",
                        hoplites: [{x: 1, y: 0, hasMoved: true}],
                        gold: 5
                    },
                    {
                        name: "player2",
                        hoplites: [{x: 1, y: 0, hasMoved: true}]
                    }
                ],
                phase: PHASES.MOVE_HOPLITES_2,
                currentPlayer: 1
            });
            const nextState = gameReducer(state, {type: 'end_turn', user: "player2"});
            expect(nextState.get('phase')).to.equal(PHASES.DEAL_CARD); 
        });

        it('when there is conflicts to resolve those phases are not skipped', () => {
            const state = fromJS({
                players:
                [
                    {
                        name: "player1",
                        hoplites: [{x: 1, y: 1, hasMoved: true}, {x: 1, y: 1, hasMoved: true, isMercenary: true}]
                    },
                    {
                        name: "player2",
                        hoplites: [{x: 1, y: 1, hasMoved: true}]
                    }
                ],
                phase: PHASES.MOVE_HOPLITES_1,
                currentPlayer: 1
            });
            const nextState = gameReducer(state, {type: 'end_turn', user: "player2"});
            expect(nextState.get('phase')).to.equal(PHASES.SOLVE_CONFLICTS_1); 
        });

        it('when there is no hoplites to move those phases are skipped', () => {
            const state = fromJS({
                players:
                [
                    {
                        name: "player1",
                        settlers: [{x: 1, y: 0, hasMoved: true}],
                        gold: 5
                    },
                    {
                        name: "player2",
                        settlers: [{x: 1, y: 0, hasMoved: true}]
                    }
                ],
                secret: {
                    deck: [
                        {id: 'Mutiny', lifespan: 5},
                        {id: 'Mercenary', lifespan: 1}
                    ]
                },
                phase: PHASES.MOVE_SETTLERS,
                currentPlayer: 1
            });
            const nextState = gameReducer(state, {type: 'end_turn', user: "player2"});
            expect(nextState.get('phase')).to.equal(PHASES.DEAL_CARD);
        });
    });
    describe('card phases -', () => {
        it('the card counters are updated without interaction from the players', () => {
            const state = fromJS({
                players:
                [
                    {
                        name: "player1",cards: [{name: 'some card'}]},
                    {
                        name: "player2"
                    }
                ],
                phase: PHASES.DEAL_CARD,
                currentPlayer: 1
            });
            const nextState = gameReducer(state, {type: 'end_turn', user: "player2"});
            expect(nextState.get('phase')).to.equal(PHASES.ACTIVATE_CARDS);
        });

        it('the deal card phase is skipped when no player has 3 gold', () => {
            const state = fromJS({
                players:
                [
                    {
                        name: "player1",
                        cards: [{name: 'some card'}],
                        gold: 1
                    },
                    {
                        name: "player2",
                        gold: 2
                    }
                ],
                phase: PHASES.SOLVE_CONFLICTS_2,
                currentPlayer: 1,
                solvis: 0
            });
            const nextState = gameReducer(state, {type: 'end_turn', user: "player1"});
            expect(nextState.get('phase')).to.equal(PHASES.ACTIVATE_CARDS);
        });

        it('the deal card phase is not skipped when a player has 3 gold', () => {
            const state = fromJS({
                players:
                [
                    {
                        name: "player1",
                        gold: 4
                    },
                    {
                        name: "player2",
                        gold: 2
                    }
                ],
                secret: {
                    deck: [
                        {id: 'Mutiny', lifespan: 5},
                        {id: 'Mercenary', lifespan: 1}
                    ]
                },
                phase: PHASES.SOLVE_CONFLICTS_2,
                currentPlayer: 1,
                solvis: 0                
            });
            const nextState = gameReducer(state, {type: 'end_turn', user: "player1"});
            expect(nextState.get('phase')).to.equal(PHASES.DEAL_CARD);
        });

        it('players which cannot bid is skipped', () => {
            const state = fromJS({
                players:
                [
                    {
                        name: "player1",
                        gold: 2
                    },
                    {
                        name: "player2",
                        gold: 5
                    },
                    {
                        name: "player3",
                        gold: 4
                    }
                ],
                secret: {
                    deck: [
                        {id: 'Mutiny', lifespan: 5},
                        {id: 'Mercenary', lifespan: 1}
                    ]
                },
                phase: PHASES.SOLVE_CONFLICTS_2,
                currentPlayer: 1,
                solvis: 2            
            });
            let nextState = gameReducer(state, {type: 'end_turn', user: "player3"});
            expect(nextState.get('phase')).to.equal(PHASES.DEAL_CARD);
            expect(nextState.get('currentPlayer')).to.equal(1);
            
            nextState = gameReducer(nextState, {type: 'bid_on_card', gold: 3, user: "player2"});
            expect(nextState.get('phase')).to.equal(PHASES.DEAL_CARD);
            expect(nextState.get('currentPlayer')).to.equal(2);

            nextState = gameReducer(nextState, {type: 'bid_on_card', gold: 4, user: "player3"});
            expect(nextState.get('phase')).to.equal(PHASES.DEAL_CARD);
            expect(nextState.get('currentPlayer')).to.equal(1);
        });

        it('the deal card phase continue until the bidding is completed', () => {
            const state = fromJS({
                players:
                [
                    {
                        name: "player1",
                        gold: 5
                    },
                    {
                        name: "player2",
                        gold: 4
                    },
                    {
                        name: "player3",
                        gold: 10
                    }
                ],
                secret: {
                    deck: [
                        {id: 'Mutiny', lifespan: 5},
                        {id: 'Mercenary', lifespan: 1}
                    ]
                },
                phase: PHASES.SOLVE_CONFLICTS_2,
                currentPlayer: 1,
                solvis: 0
            });
            let nextState = gameReducer(state, {type: 'end_turn', user: "player1"});    
            expect(nextState.get('phase')).to.equal(PHASES.DEAL_CARD);
            expect(nextState.get('currentPlayer')).to.equal(0);
            expect(nextState.get('shownCard')).to.equal(fromJS({id: "Mutiny", lifespan: 5}));
            expect(nextState.get('cardBid')).to.be.undefined;
            
            nextState = gameReducer(nextState, {type: 'bid_on_card', gold: 3, user: "player1"});
            expect(nextState.get('phase')).to.equal(PHASES.DEAL_CARD);
            expect(nextState.get('currentPlayer')).to.equal(1);
            expect(nextState.get('cardBid')).to.equal(fromJS({playerIndex: 0, gold: 3}));
            
            nextState = gameReducer(nextState, {type: 'bid_on_card', gold: 4, user: "player2"});
            expect(nextState.get('phase')).to.equal(PHASES.DEAL_CARD);
            expect(nextState.get('currentPlayer')).to.equal(2);
            expect(nextState.get('cardBid')).to.equal(fromJS({playerIndex: 1, gold: 4}));
            
            nextState = gameReducer(nextState, {type: 'bid_on_card', gold: 5, user: "player3"});
            expect(nextState.get('phase')).to.equal(PHASES.ACTIVATE_CARDS);
            expect(nextState.get('currentPlayer')).to.equal(2);
            expect(nextState.get('cardBid')).to.be.undefined;
            expect(nextState.get('shownCard')).to.be.undefined;
        });

        it('the play card phase is skipped when a player has no card', () => {
            const state = fromJS({
                players:
                [
                    {
                        name: "player1",
                        cards: [],
                        gold: 2,
                        food: 1
                    },
                    {
                        name: "player2",
                        cards: [{name: 'some card'}]
                    }
                ],
                phase: PHASES.CARD_COUNTERS,
                currentPlayer: 1
            });
            const nextState = gameReducer(state, {type: 'end_turn', user: "player2"});    
            expect(nextState.get('phase')).to.equal(PHASES.ACTIVATE_CARDS);
            expect(nextState.get('currentPlayer')).to.equal(1);
        });

        it('the play card phase is not skipped when a player has 3 gold', () => {
            const state = fromJS({
                players:
                [
                    {
                        name: "player1",
                        cards: [{name: 'some card'}]
                    },
                    {
                        name: "player2",
                        cards: []
                    }
                ],
                phase: PHASES.CARD_COUNTERS,
                currentPlayer: 1
            });
            const nextState = gameReducer(state, {type: 'end_turn', user: "player2"});    
            expect(nextState.get('phase')).to.equal(PHASES.ACTIVATE_CARDS);
        });
    });
    describe('income/expense phases -', () => {
        it('the income phase is done automatically', () => {
            const state = fromJS({
                players:
                [
                    {
                        name: "player1",
                        hoplites: [{x: 1, y:1, isMercenary: true}]
                    },
                    {
                        name: "player2",
                    }
                ],
                phase: PHASES.RECRUIT,
                currentPlayer: 1
            });
            const nextState = gameReducer(state, {type: 'end_turn', user: "player2"});    
            expect(nextState.get('phase')).to.equal(PHASES.PAY_MERCENARIES);
        });

        it('the pay mercenary phase is skipped when player has no mercenaries', () => {
            const state = fromJS({
                players:
                [
                    {
                        name: "player1",
                        hoplites: [{x: 1, y:1, isMercenary: true, payed: true}]
                    },
                    {
                        name: "player2",
                        settlers: [{x: 0, y: 0}]
                    }
                ],
                phase: PHASES.PAY_MERCENARIES,
                currentPlayer: 0
            });
            const nextState = gameReducer(state, {type: 'end_turn', user: "player1"});    
            expect(nextState.get('phase')).to.equal(PHASES.MOVE_SETTLERS);
        });

        it('the payed flag is resetted after the pay mercenary phase', () => {
            const state = fromJS({
                players:
                [
                    {
                        name: "player1",
                        hoplites: [{x: 1, y:1, isMercenary: true, payed: true}]
                    },
                    {
                        name: "player2",
                        settlers: [{x: 0, y: 0}]
                    }
                ],
                phase: PHASES.PAY_MERCENARIES,
                currentPlayer: 0
            });
            const nextState = gameReducer(state, {type: 'end_turn', user: "player1"});    
            expect(nextState.get('players').get(0).get('hoplites').get(0)).to.equal(fromJS({x: 1, y:1, isMercenary: true}));
        });

        it('the pay mercenary phase cannot be ended when not all mercenaries are managed', () => {
            const state = fromJS({
                players:
                [
                    {
                        name: "player1",
                        hoplites: [{x: 1, y:1, isMercenary: true}]
                    },
                    {
                        name: "player2",
                        settlers: [{x: 0, y: 0}]
                    }
                ],
                phase: PHASES.PAY_MERCENARIES,
                currentPlayer: 0
            });
            const nextState = gameReducer(state, {type: 'end_turn', user: "player1"});    
            expect(nextState.get('phase')).to.equal(PHASES.PAY_MERCENARIES);
            expect(nextState.get('currentPlayer')).to.equal(0);
        });
    });
});
