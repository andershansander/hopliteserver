import {List, Map, toJS, fromJS} from 'immutable';
import {expect} from 'chai';

import {collectIncome} from '../src/game_logic/phases/income.js';
import PHASES from '../src/game_logic/phases.js';

describe('caravan income -', () => {
  it('gets income when 2 steps north east of neutral village', () => {
    const state = fromJS({
        map: [
            [  'D', 'D', 'D', 'D', 'D'], 
            ['D', 'D', 'D', 'D', 'D'],
            [  'D', 'D', 'D', 'D', 'D'], 
            ['D', 'D', 'D', 'D', 'D'],
            [  'D', 'V', 'D', 'D', 'D'],
        ],
        players:[{cities: [{x: 2, y: 2, hasCaravan: true}], gold: 0}],
        phase: PHASES.INCOME,
        currentPlayer: 0
      });
    const nextState = collectIncome(state);
    expect(nextState.get('players').get(0).get('gold')).to.equal(1);    
  });

  it('gets income when 2 steps south east of neutral village', () => {
    const state = fromJS({
        map: [
            [  'D', 'V', 'D', 'D', 'D'], 
            ['D', 'D', 'D', 'D', 'D'],
            [  'D', 'D', 'D', 'D', 'D'], 
            ['D', 'D', 'D', 'D', 'D'],
            [  'D', 'D', 'D', 'D', 'D'],
        ],
        players:[{cities: [{x: 2, y: 2, hasCaravan: true}], gold: 0}],
        phase: PHASES.INCOME,
        currentPlayer: 0
      });
    const nextState = collectIncome(state);
    expect(nextState.get('players').get(0).get('gold')).to.equal(1);    
  });

  it('gets income when 2 steps north west of neutral village', () => {
    const state = fromJS({
        map: [
            [  'D', 'D', 'D', 'V', 'D'], 
            ['D', 'D', 'D', 'D', 'D'],
            [  'D', 'D', 'D', 'D', 'D'], 
            ['D', 'D', 'D', 'D', 'D'],
            [  'D', 'D', 'D', 'D', 'D'],
        ],
        players:[{cities: [{x: 2, y: 2, hasCaravan: true}], gold: 0}],
        phase: PHASES.INCOME,
        currentPlayer: 0
      });
    const nextState = collectIncome(state);
    expect(nextState.get('players').get(0).get('gold')).to.equal(1);    
  });

  it('gets income when 2 steps south west of neutral village', () => {
    const state = fromJS({
        map: [
            [  'D', 'D', 'D', 'D', 'D'], 
            ['D', 'D', 'D', 'D', 'D'],
            [  'D', 'D', 'D', 'D', 'D'], 
            ['D', 'D', 'D', 'D', 'D'],
            [  'D', 'D', 'D', 'V', 'D'],
        ],
        players:[{cities: [{x: 2, y: 2, hasCaravan: true}], gold: 0}],
        phase: PHASES.INCOME,
        currentPlayer: 0
      });
    const nextState = collectIncome(state);
    expect(nextState.get('players').get(0).get('gold')).to.equal(1);    
  });

  it('gets no income when city lacks caravan', () => {
    const state = fromJS({
        map: [
            [  'D', 'D', 'D', 'D', 'D'], 
            ['D', 'D', 'D', 'D', 'D'],
            [  'D', 'D', 'D', 'D', 'D'], 
            ['D', 'D', 'D', 'D', 'D'],
            [  'D', 'D', 'D', 'V', 'D'],
        ],
        players:[{cities: [{x: 2, y: 2, hasCaravan: false}], gold: 0}],
        phase: PHASES.INCOME,
        currentPlayer: 0
      });
    const nextState = collectIncome(state);
    expect(nextState.get('players').get(0).get('gold')).to.equal(0);    
  });

  it('gets no income when enemy hoplite blocks neutral village', () => {
    const state = fromJS({
        map: [
            [  'D', 'D', 'D', 'D', 'D'],
            ['D', 'D', 'D', 'D', 'D'],
            [  'D', 'D', 'D', 'D', 'D'], 
            ['D', 'D', 'D', 'D', 'D'],
            [  'D', 'D', 'D', 'V', 'D'],
        ],
        players:[{cities: [{x: 2, y: 2, hasCaravan: true}], gold: 0},{hoplites: [{x: 3, y: 4}]}],
        phase: PHASES.INCOME,
        currentPlayer: 0
      });
    const nextState = collectIncome(state);
    expect(nextState.get('players').get(0).get('gold')).to.equal(0);    
  });

  it('gets no income when to far west of neutral village', () => {
    const state = fromJS({
        map: [
            [  'D', 'D', 'D', 'D', 'D'],
            ['D', 'D', 'D', 'D', 'D'],
            [  'D', 'D', 'D', 'V', 'D'], 
            ['D', 'D', 'D', 'D', 'D'],
            [  'D', 'D', 'D', 'D', 'D'],
        ],
        players:[{cities: [{x: 0, y: 2, hasCaravan: true}], gold: 0}],
        phase: PHASES.INCOME,
        currentPlayer: 0
      });
    const nextState = collectIncome(state);
    expect(nextState.get('players').get(0).get('gold')).to.equal(0);    
  });

  it('gets no income when to far east of neutral village', () => {
    const state = fromJS({
        map: [
            [  'D', 'D', 'D', 'D', 'D'],
            ['D', 'D', 'D', 'D', 'D'],
            [  'D', 'V', 'D', 'D', 'D'], 
            ['D', 'D', 'D', 'D', 'D'],
            [  'D', 'D', 'D', 'D', 'D'],
        ],
        players:[{cities: [{x: 4, y: 2, hasCaravan: true}], gold: 0}],
        phase: PHASES.INCOME,
        currentPlayer: 0
      });
    const nextState = collectIncome(state);
    expect(nextState.get('players').get(0).get('gold')).to.equal(0);    
  });

  it('gets no income when to far north north west of neutral village', () => {
    const state = fromJS({
        map: [
            [  'D', 'D', 'D', 'D', 'D'],
            ['D', 'D', 'D', 'D', 'D'],
            [  'D', 'D', 'V', 'D', 'D'], 
            ['D', 'D', 'D', 'D', 'D'],
            [  'D', 'D', 'D', 'D', 'D'],
        ],
        players:[{cities: [{x: 0, y: 0, hasCaravan: true}], gold: 0}],
        phase: PHASES.INCOME,
        currentPlayer: 0
      });
    const nextState = collectIncome(state);
    expect(nextState.get('players').get(0).get('gold')).to.equal(0);    
  });

  it('gets no income when to far south south east of neutral village', () => {
    const state = fromJS({
        map: [
            [  'D', 'D', 'D', 'D', 'D'],
            ['D', 'D', 'D', 'D', 'D'],
            [  'D', 'D', 'V', 'D', 'D'], 
            ['D', 'D', 'D', 'D', 'D'],
            [  'D', 'D', 'D', 'D', 'D'],
        ],
        players:[{cities: [{x: 4, y: 4, hasCaravan: true}], gold: 0}],
        phase: PHASES.INCOME,
        currentPlayer: 0
      });
    const nextState = collectIncome(state);
    expect(nextState.get('players').get(0).get('gold')).to.equal(0);    
  });

  it('can only trade once with each village', () => {
    const state = fromJS({
        map: [
            [  'D', 'D', 'D', 'D', 'D'],
            ['D', 'D', 'D', 'D', 'D'],
            [  'D', 'D', 'V', 'D', 'D'], 
            ['D', 'D', 'D', 'D', 'D'],
            [  'D', 'D', 'D', 'D', 'D'],
        ],
        players:[{cities: [{x: 3, y: 3, hasCaravan: true}, {x: 3, y: 2, hasCaravan: true}], gold: 0}],
        phase: PHASES.INCOME,
        currentPlayer: 0
      });
    const nextState = collectIncome(state);
    expect(nextState.get('players').get(0).get('gold')).to.equal(1);
  });
});
