import {List, Map, toJS, fromJS} from 'immutable';
import {expect} from 'chai';

import {removeHoplites, allConflictsSolved} from '../src/game_logic/phases/solve_conflicts.js';
import gameReducer from '../src/reducers/game_reducer.js';
import PHASES from '../src/game_logic/phases.js';

describe('solve conflicts -', () => {
    it('winner can remove his lost hoplites', () => {
        const state = fromJS({
            map: [
                ['S', 'R'], 
                ['S', 'V']
            ],
            players:
            [
                {
                    hoplites: [
                        {x: 1, y: 0},
                        {x: 1, y: 0},
                        {x: 1, y: 0},
                    ],
                    hoplitesToRemove: [
                        {location: {x: 1, y: 0}, amount: 2}
                    ]
                },
                {
                    hoplites: []
                }
            ],
            phase: PHASES.SOLVE_CONFLICTS_1,
            currentPlayer: 0,
            solvis: 0
        });
        const nextState = removeHoplites(state, {
            hopliteIndices: [0, 1]
        });
        expect(nextState.get('players').get(0).get('hoplitesToRemove')).to.equal(List());
        expect(nextState.get('players').get(0).get('hoplites')).to.equal(fromJS([{x: 1, y: 0}]));
        expect(allConflictsSolved(nextState)).to.be.true;
    });

    it('winner can remove his lost hoplites when the other player initiated the battle', () => {
        const state = fromJS({
            map: [
                ['S', 'R'], 
                ['S', 'V']
            ],
            players:
            [
                {
                    hoplites: [
                        {x: 1, y: 0, hasMoved: true}
                    ]
                },
                {
                    hoplites: [
                        {x: 1, y: 0},
                        {x: 1, y: 0}
                    ]
                }
            ],
            phase: PHASES.MOVE_HOPLITES_2,
            currentPlayer: 0
        });
        const nextState = gameReducer(state, {type: 'end_turn'});

        expect(nextState.get('players').get(0).get('hoplites')).to.equal(fromJS([]));
        expect(nextState.get('players').get(1).get('hoplites')).to.equal(fromJS([
                        {x: 1, y: 0},
                        {x: 1, y: 0}
        ]));
        expect(nextState.get('players').get(1).get('hoplitesToRemove')).to.equal(fromJS([{location: {x: 1, y: 0}, amount: 1}]));
        expect(nextState.get('solvis')).to.equal(1);
        expect(nextState.get('phase')).to.equal(PHASES.SOLVE_CONFLICTS_2);
        expect(allConflictsSolved(nextState)).to.be.false;
    });

    it('winner can not remove hoplite at wrong location', () => {
        const state = fromJS({
            map: [
                ['S', 'R'], 
                ['S', 'V']
            ],
            players:
            [
                {
                    hoplites: [
                        {x: 1, y: 0},
                        {x: 1, y: 0},
                        {x: 1, y: 1},
                    ],
                    hoplitesToRemove: [
                        {location: {x: 1, y: 0}, amount: 2}
                    ]
                },
                {
                    hoplites: []
                }
            ],
            phase: PHASES.SOLVE_CONFLICTS_1,
            currentPlayer: 0,
            solvis: 0
        });
        const nextState = removeHoplites(state, {
            hopliteIndices: [0, 2]
        });
        expect(nextState.get('players').get(0).get('hoplitesToRemove')).to.equal(fromJS([{location: {x: 1, y: 0}, amount: 1}]));
        expect(nextState.get('players').get(0).get('hoplites')).to.equal(fromJS([{x: 1, y: 0}, {x: 1, y: 1}]));
        expect(allConflictsSolved(nextState)).to.be.false;
    });
});
