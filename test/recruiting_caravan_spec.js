import {List, Map, toJS, fromJS} from 'immutable';
import {expect} from 'chai';

import {recruitCaravan} from '../src/game_logic/phases/recruit.js';
import PHASES from '../src/game_logic/phases.js';

describe('recruiting caravan -', () => {
  it('can recruit caravan in a friendly town', () => {
    const state = fromJS({
        players:
        [
            { 
                gold: 3,
                cities: [{x: 0, y: 0, hasCaravan: false}],
                food: 6,
            },            
            {
                gold: 0,
                cities: [{x: 0, y: 1, hasCaravan: false}],
                food: 0
            }
        ],
        phase: PHASES.RECRUIT,
        currentPlayer: 0
      });
    const nextState = recruitCaravan(state, {target: {x: 0, y: 0}, cost: {food: 2, gold: 2}});
    
    expect(nextState.get('players').get(0).get('gold')).to.equal(1);
    expect(nextState.get('players').get(0).get('food')).to.equal(4);      
    expect(nextState.get('players').get(0).get('cities').get(0).get('hasCaravan')).to.be.true;
  });
  
  it('cannot recruit caravans in wrong phase', () => {
    const state = fromJS({
        players:
        [
            { 
                gold: 3,
                cities: [{x: 0, y: 0, hasCaravan: false}],
                food: 6,
            },            
            {
                gold: 0,
                cities: [{x: 0, y: 1, hasCaravan: false}],
                food: 0
            }
        ],
        phase: PHASES.MOVE_SETTLERS,
        currentPlayer: 0
      });
    const nextState = recruitCaravan(state, {target: {x: 0, y: 0}, cost: {food: 2, gold: 2}});
    
    expect(nextState.get('players').get(0).get('gold')).to.equal(3);
    expect(nextState.get('players').get(0).get('food')).to.equal(6);      
    expect(nextState.get('players').get(0).get('cities').get(0).get('hasCaravan')).to.be.false;  
  });
  
  it('cannot have two caravans in a friendly town', () => {
    const state = fromJS({
        players:
        [
            { 
                gold: 3,
                cities: [{x: 0, y: 0, hasCaravan: true}],
                food: 6,
            },            
            {
                gold: 0,
                cities: [{x: 0, y: 1, hasCaravan: false}],
                food: 0
            }
        ],
        phase: PHASES.RECRUIT,
        currentPlayer: 0
      });
    const nextState = recruitCaravan(state, {target: {x: 0, y: 0}, cost: {food: 2, gold: 2}});
    
    expect(nextState.get('players').get(0).get('gold')).to.equal(3);
    expect(nextState.get('players').get(0).get('food')).to.equal(6);      
    expect(nextState.get('players').get(0).get('cities').get(0).get('hasCaravan')).to.be.true;  
  });
  
  it('cannot recruit caravan in a enemy town', () => {
    const state = fromJS({
        players:
        [
            { 
                gold: 3,
                cities: [{x: 0, y: 0, hasCaravan: false}],
                food: 6,
            },            
            {
                gold: 0,
                cities: [{x: 0, y: 1, hasCaravan: false}],
                food: 0
            }
        ],
        phase: PHASES.RECRUIT,
        currentPlayer: 0
      });
    const nextState = recruitCaravan(state, {target: {x: 0, y: 1}, cost: {food: 2, gold: 2}});
    
    expect(nextState.get('players').get(0).get('gold')).to.equal(3);
    expect(nextState.get('players').get(0).get('food')).to.equal(6);      
    expect(nextState.get('players').get(0).get('cities').get(0).get('hasCaravan')).to.be.false;
  });
  
  it('cannot recruit caravan when short of food', () => {
    const state = fromJS({
        players:
        [
            { 
                gold: 3,
                cities: [{x: 0, y: 0, hasCaravan: false}],
                food: 1,
            },            
            {
                gold: 0,
                cities: [{x: 0, y: 1, hasCaravan: false}],
                food: 0
            }
        ],
        phase: PHASES.RECRUIT,
        currentPlayer: 0
      });
    const nextState = recruitCaravan(state, {target: {x: 0, y: 0}, cost: {food: 2, gold: 2}});
    
    expect(nextState.get('players').get(0).get('gold')).to.equal(3);
    expect(nextState.get('players').get(0).get('food')).to.equal(1);      
    expect(nextState.get('players').get(0).get('cities').get(0).get('hasCaravan')).to.be.false;
  });
  
  it('cannot recruit caravan when short of gold', () => {
    const state = fromJS({
        players:
        [
            { 
                gold: 1,
                cities: [{x: 0, y: 0, hasCaravan: false}],
                food: 6,
            },            
            {
                gold: 0,
                cities: [{x: 0, y: 1, hasCaravan: false}],
                food: 0
            }
        ],
        phase: PHASES.RECRUIT,
        currentPlayer: 0
      });
    const nextState = recruitCaravan(state, {target: {x: 0, y: 0}, cost: {food: 2, gold: 2}});
    
    expect(nextState.get('players').get(0).get('gold')).to.equal(1);
    expect(nextState.get('players').get(0).get('food')).to.equal(6);      
    expect(nextState.get('players').get(0).get('cities').get(0).get('hasCaravan')).to.be.false;
  });
});
