import {List, Map, toJS, fromJS} from 'immutable';
import {expect} from 'chai';

import {onEnterPhase, bidOnShownCard, passOnShownCard} from '../src/game_logic/phases/deal_card.js';
import {updateCardCounters} from '../src/game_logic/phases/card_counters.js';
import {playCard} from '../src/game_logic/phases/activate_cards.js';
import {generateDeck} from '../src/game_logic/utilities/deck.js';

import PHASES from '../src/game_logic/phases.js';

describe('card spec -', () => {
    it('the generated deck has the correct number of cards', () => {            
        const deck = generateDeck();
        expect(deck.size).to.equal(32);            
    });

    it('the generated deck has two mutiny cards', () => {            
        const deck = generateDeck();
        const mutinyCards = deck.filter(card => card.get('id') === 'Mutiny');
        expect(mutinyCards.size).to.equal(2);            
    });

    it('the generated deck has four mercenary cards', () => {            
        const deck = generateDeck();
        const mercenaryCards = deck.filter(card => card.get('id') === 'Mercenary');
        expect(mercenaryCards.size).to.equal(4);         
    });


    it('the top card in the deck can be shown', () => {            
        const state = fromJS({
            secret: {
                deck: [
                    {id: "Mutiny", lifespan: 5},
                    {id: "Mercenary"}
                ]
            },
            players: [
                {name: "Player1", gold: 3}
            ],
            phase: PHASES.DEAL_CARD,
            currentPlayer: 0
        });
        
        const nextState = onEnterPhase(state);
        expect(nextState.get('shownCard')).to.equal(fromJS({id: "Mutiny", lifespan: 5}));
        expect(nextState.getIn(['secret', 'deck']).size).to.equal(1);
    });

    it('the shown card may be bought', () => {            
        const state = fromJS({
            players: [{
                gold: 4
            }],
            secret: {
                deck: [
                    {id: "Mutiny", lifespan: 5},
                    {id: "Mercenary"}
                ]
            },
            phase: PHASES.DEAL_CARD,
            currentPlayer: 0
        });
        
        let nextState = onEnterPhase(state);
        nextState = bidOnShownCard(nextState, {playerIndex: 0, gold: 3});
        expect(nextState.get('cardBid')).to.be.undefined;
        expect(nextState.get('shownCard')).to.be.undefined;
        expect(nextState.getIn(['secret', 'deck']).size).to.equal(1);
    });

    it('the deck is shuffled and reused when emptied', () => {            
        const state = fromJS({
            players: [{
                gold: 4
            }],
            secret: {
                deck: [
                    {id: "Mutiny", lifespan: 5}
                ],
                discardedCards: [
                    {id: "Mercenary"},
                    {id: "Call to arms"}
                ]
            },
            phase: PHASES.DEAL_CARD,
            currentPlayer: 0
        });
        
        let nextState = onEnterPhase(state);
        nextState = bidOnShownCard(nextState, {playerIndex: 0, gold: 3});
        const deck = nextState.getIn(['secret', 'deck']);
        expect(deck.size).to.equal(2);
        expect(deck.filter(card => card.get('id') === 'Mercenary').size).to.equal(1);
        expect(deck.filter(card => card.get('id') === 'Call to arms').size).to.equal(1);
    });

    it('the deck is shuffled and reused when there is a single card which gets discarded', () => {            
        const state = fromJS({
            players: [{
                gold: 2
            }],
            secret: {
                deck: [
                    {id: "Mutiny", lifespan: 5}
                ]
            },
            phase: PHASES.DEAL_CARD,
            currentPlayer: 0
        });
        
        let nextState = onEnterPhase(state);
        nextState = passOnShownCard(nextState, {playerIndex: 0});
        const deck = nextState.getIn(['secret', 'deck']);
        expect(deck.size).to.equal(1);
        expect(deck.filter(card => card.get('id') === 'Mutiny').size).to.equal(1);
    });

    it('card is discarded if no player bids', () => {            
        const state = fromJS({
            players: [{
                gold: 4
            }],
            secret: {
                deck: [
                    {id: "Mutiny", lifespan: 5},
                    {id: "Mercenary"},
                ]
            },
            phase: PHASES.DEAL_CARD,
            currentPlayer: 0
        });
        
        let nextState = onEnterPhase(state);
        nextState = passOnShownCard(nextState, {playerIndex: 0});
        const deck = nextState.getIn(['secret', 'deck']);
        const discarded = nextState.getIn(['secret', 'discardedCards']);
        expect(deck.size).to.equal(1);
        expect(deck.filter(card => card.get('id') === 'Mercenary').size).to.equal(1);        
        expect(nextState.get('shownCard')).to.be.undefined;
        expect(discarded.size).to.equal(1);
        expect(discarded.filter(card => card.get('id') === 'Mutiny').size).to.equal(1);
    });

    it('a player may activate a card', () => {            
        const state = fromJS({
            players: [{
                cards: [
                     {id: "Mutiny", lifespan: 5}
                ]
            }],
            phase: PHASES.ACTIVATE_CARDS,
            currentPlayer: 0
        });
        
        let nextState = playCard(state, {playerIndex: 0, cardIndex: 0});
        expect(nextState.get('activeCards')).to.equal(fromJS(
            [{id: "Mutiny", playedByPlayerIndex: 0, lifespan: 5, round: 1}]
        ));
    });

    it('played cards get their counters updated', () => {            
        const state = fromJS({
            players: [{
                cards: [
                     {id: "Mutiny", lifespan: 5}
                ]
            }],
            phase: PHASES.ACTIVATE_CARDS,
            currentPlayer: 0
        });
        
        let nextState = playCard(state, {playerIndex: 0, cardIndex: 0});
        nextState = updateCardCounters(nextState);
        expect(nextState.get('activeCards')).to.equal(fromJS(
            [{id: "Mutiny", playedByPlayerIndex: 0, lifespan: 5, round: 2}]
        ));
    });

    it('expiring cards are discarded', () => {            
        const state = fromJS({
            players: [{
            }],
            activeCards: [
                {id: "Mutiny", playedByPlayerIndex: 0, lifespan: 2, round: 1}
            ],
            phase: PHASES.ACTIVATE_CARDS
        });
        
        let nextState = updateCardCounters(state);
        expect(nextState.get('activeCards')).to.equal(fromJS(
            [{id: "Mutiny", playedByPlayerIndex: 0, lifespan: 2, round: 2}]
        ));
        nextState = updateCardCounters(nextState);
        expect(nextState.get('activeCards')).to.equal(fromJS(
            []
        ));
        expect(nextState.getIn(['secret', 'discardedCards'])).to.equal(fromJS(
            [{id: "Mutiny", lifespan: 2}]
        ));
    });
});