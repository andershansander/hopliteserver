import {List, Map, toJS} from 'immutable';
import {expect} from 'chai';

import {startGame} from '../src/game_logic/start_game.js';

describe('initialization logic -', () => {
  it('initializes counters', () => {
    const state = Map();
    const players = ['Linda', 'Gridde', 'Ville', ];
    const nextState = startGame(state, {players});
    expect(nextState.get('phase')).to.equal(1);
    expect(nextState.get('currentPlayer')).to.equal(0);
  });

  it('creates a map', () => {
    const state = Map();
    const players = ['Linda', 'Gridde', 'Ville', ];
    const nextState = startGame(state, {players});
    expect(nextState.get('map')).to.exist;
  });

  it('creates a deck in the states secret part', () => {
    const state = Map();
    const players = ['Linda', 'Gridde', 'Ville', ];
    const nextState = startGame(state, {players});
    expect(nextState.getIn('secret', 'deck')).to.exist;
  });

  it('creates players', () => {
    const state = Map();
    const players = ['Linda', 'Gridde', 'Ville', 'Pogge'];
    const nextState = startGame(state, {players});
    expect(nextState.get('players').toJS().map(player => player.name)).to.eql([
      'Linda', 'Gridde', 'Ville', 'Pogge']);
  });

  it('created players have one city and one settler at a starting location', () => {
    const state = Map();
    const players = ['Linda', 'Gridde'];
    const nextState = startGame(state, {players});
    expect(nextState.get('players').get(0).get('cities').size).to.equal(1);
    expect(nextState.get('players').get(1).get('cities').size).to.equal(1);

    const player1City = nextState.get('players').get(0).get('cities').get(0);
    const player1Settler = nextState.get('players').get(0).get('settlers').get(0);
    expect(nextState.get('map').get(player1City.get('y')).get(player1City.get('x'))).to.equal('S');
    expect(player1City.get('x')).to.equal(player1Settler.get('x'));
    expect(player1City.get('y')).to.equal(player1Settler.get('y'));

    
    const player2City = nextState.get('players').get(1).get('cities').get(0);
    const player2Settler = nextState.get('players').get(1).get('settlers').get(0);
    expect(nextState.get('map').get(player2City.get('y')).get(player2City.get('x'))).to.equal('S');
    expect(player2City.get('x')).to.equal(player2Settler.get('x'));
    expect(player2City.get('y')).to.equal(player2Settler.get('y'));
    
    expect(player1City.toJS()).to.not.equal(player2City.toJS());
  });
});
