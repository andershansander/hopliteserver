import {List, Map, toJS, fromJS} from 'immutable';
import {expect} from 'chai';

import {moveSettler} from '../src/game_logic/phases/move_settlers.js';
import {moveHoplite} from '../src/game_logic/phases/move_hoplites.js';
import PHASES from '../src/game_logic/phases.js';

const TEST_MAP = [
            [   'S', 'R', 'F'], 
            ['S', 'V', 'F', 'M'],
            [   'M', 'P', 'D']
        ];
        
const BIG_TEST_MAP = [
            [  'D', 'D', 'D', 'D', 'D'],
            ['D', 'D', 'D', 'D', 'D'],
            [  'D', 'D', 'D', 'D', 'D'], 
            ['D', 'D', 'D', 'D', 'D'],
            [  'D', 'D', 'D', 'V', 'D'],
            ['D', 'D', 'D', 'D', 'D'],
            [  'D', 'D', 'D', 'V', 'D'],
];
describe('move units -', () => {
  it('settlers can move to adjacent squares', () => {
    const state = fromJS({
        map: TEST_MAP,
        players:
        [
            {
                settlers: [{x: 1, y: 0}]
            }
        ],
        phase: PHASES.MOVE_SETTLERS,
        currentPlayer: 0
      });
    const nextState = moveSettler(state, {playerIndex: 0, settlerIndex: 0, target: {x: 2, y: 1}});
    expect(nextState.get('players').get(0).get('settlers').get(0)).to.equal(Map({x: 2, y: 1, hasMoved: true}));
  });

  it('settlers can move from 2,6 to 3,5', () => {
    const state = fromJS({
        map: BIG_TEST_MAP,
        players:
        [
            {
                settlers: [{x: 2, y: 6}]
            }
        ],
        phase: PHASES.MOVE_SETTLERS,
        currentPlayer: 0
      });
    const nextState = moveSettler(state, {playerIndex: 0, settlerIndex: 0, target: {x: 3, y: 5}});
    expect(nextState.get('players').get(0).get('settlers').get(0)).to.equal(Map({x: 3, y: 5, hasMoved: true}));
  });

  it('settlers cannot move up and two to the left when standing on even row', () => {
    const state = fromJS({
        map: TEST_MAP,
        players:
        [
            {
                settlers: [{x: 1, y: 2}]
            }
        ],
        phase: PHASES.MOVE_SETTLERS,
        currentPlayer: 0
      });
    const nextState = moveSettler(state, {playerIndex: 0, settlerIndex: 0, target: {x: 0, y: 1}});
    expect(nextState.get('players').get(0).get('settlers').get(0)).to.equal(Map({x: 1, y: 2}));
  });

  it('settlers can move up and to the right when on odd row', () => {
    const state = fromJS({
        map: TEST_MAP,
        players:
        [
            {
                settlers: [{x: 2, y: 2}]
            }
        ],
        phase: PHASES.MOVE_SETTLERS,
        currentPlayer: 0
      });
    const nextState = moveSettler(state, {playerIndex: 0, settlerIndex: 0, target: {x: 3, y: 1}});
    expect(nextState.get('players').get(0).get('settlers').get(0)).to.equal(Map({x: 3, y: 1, hasMoved: true}));
  });
  
  it('settlers cannot move in the hoplite phase', () => {
    const state = fromJS({
        map: TEST_MAP,
        players:
        [
            {
                settlers: [{x: 1, y: 0}]
            }
        ],
        phase: PHASES.MOVE_HOPLITES_1,
        currentPlayer: 0
      });
    const nextState = moveSettler(state, {playerIndex: 0, settlerIndex: 0, target: {x: 2, y: 1}});
    expect(nextState.get('players').get(0).get('settlers').get(0)).to.equal(Map({x: 1, y: 0}));
  });
  
  
  it('two different settlers may move in the same round', () => {
    const state = fromJS({
        map: TEST_MAP,
        players:
        [
            {
                settlers: [{x: 1, y: 0}, {x: 2, y: 0}]
            }
        ],
        phase: PHASES.MOVE_SETTLERS,
        currentPlayer: 0
      });
    let nextState = moveSettler(state, {playerIndex: 0, settlerIndex: 0, target: {x: 2, y: 1}});
    nextState = moveSettler(nextState, {playerIndex: 0, settlerIndex: 1, target: {x: 3, y: 1}});
    expect(nextState.get('players').get(0).get('settlers').get(0)).to.equal(Map({x: 2, y: 1, hasMoved: true}));
    expect(nextState.get('players').get(0).get('settlers').get(1)).to.equal(Map({x: 3, y: 1, hasMoved: true}));
  });
  
  it('settlers can not move to none adjacent squares', () => {
    const state = fromJS({
        map: TEST_MAP,
        players:
        [
            {
                settlers: [{x: 0, y: 0}]
            }
        ],
        phase: PHASES.MOVE_SETTLERS,
        currentPlayer: 0
      });
    const nextState = moveSettler(state, {playerIndex: 0, settlerIndex: 0, target: {x: 2, y: 1}});
    expect(nextState.get('players').get(0).get('settlers').get(0)).to.equal(Map({x: 0, y: 0}));
  });
  
  it('settlers cannot move twice', () => {
    const state = fromJS({
        map: TEST_MAP,
        players:
        [
            {
                settlers: [{x: 0, y: 0}]
            }
        ],
        phase: PHASES.MOVE_SETTLERS,
        currentPlayer: 0
      });
    let nextState = moveSettler(state, {playerIndex: 0, settlerIndex: 0, target: {x: 1, y: 0}});
    nextState = moveSettler(nextState, {playerIndex: 0, settlerIndex: 0, target: {x: 1, y: 1}});
    expect(nextState.get('players').get(0).get('settlers').get(0)).to.equal(Map({x: 1, y: 0, hasMoved: true}));
  });

  it('settlers cannot move out of turn', () => {
    const state = fromJS({
        map: TEST_MAP,
        players:
        [
            {
                settlers: [{x: 0, y: 0}]
            },
             {
                settlers: [{x: 1, y: 1}]
            }
        ],
        phase: PHASES.MOVE_SETTLERS,
        currentPlayer: 1
      });
    let nextState = moveSettler(state, {playerIndex: 1, settlerIndex: 0, target: {x: 1, y: 0}});    
    expect(nextState.get('players').get(0).get('settlers').get(0)).to.equal(Map({x: 0, y: 0}));
  });
  
  it('hoplites can move to adjacent squares', () => {
    const state = fromJS({
        map: TEST_MAP,
        players:
        [
            {
                hoplites: [{x: 1, y: 0}]
            }
        ],
        phase: PHASES.MOVE_HOPLITES_1,
        currentPlayer: 0
      });
    const nextState = moveHoplite(state, {playerIndex: 0, hopliteIndex: 0, target: {x: 2, y: 1}});
    expect(nextState.get('players').get(0).get('hoplites').get(0)).to.equal(Map({x: 2, y: 1, hasMoved: true}));
  });
  
  it('hoplites can move in two phases', () => {
    const state = fromJS({
        map: TEST_MAP,
        players:
        [
            {
                hoplites: [{x: 1, y: 0}]
            }
        ],
        phase: PHASES.MOVE_HOPLITES_2,
        currentPlayer: 0
      });
    const nextState = moveHoplite(state, {playerIndex: 0, hopliteIndex: 0, target: {x: 2, y: 1}});
    expect(nextState.get('players').get(0).get('hoplites').get(0)).to.equal(Map({x: 2, y: 1, hasMoved: true}));
  });
  
  it('hoplites cannot move in move settlers phase', () => {
    const state = fromJS({
        map: TEST_MAP,
        players:
        [
            {
                hoplites: [{x: 1, y: 0}]
            }
        ],
        phase: PHASES.MOVE_SETTLERS,
        currentPlayer: 0
      });
    const nextState = moveHoplite(state, {playerIndex: 0, hopliteIndex: 0, target: {x: 1, y: 1}});
    expect(nextState.get('players').get(0).get('hoplites').get(0)).to.equal(Map({x: 1, y: 0}));
  });
  
  it('two different hoplites may move in the same round', () => {
    const state = fromJS({
        map: TEST_MAP,
        players:
        [
            {
                hoplites: [{x: 1, y: 0}, {x: 2, y: 0}]
            }
        ],
        phase: PHASES.MOVE_HOPLITES_1,
        currentPlayer: 0
      });
    let nextState = moveHoplite(state, {playerIndex: 0, hopliteIndex: 0, target: {x: 2, y: 1}});
    nextState = moveHoplite(nextState, {playerIndex: 0, hopliteIndex: 1, target: {x: 2, y: 1}});
    expect(nextState.get('players').get(0).get('hoplites').get(0)).to.equal(Map({x: 2, y: 1, hasMoved: true}));
    expect(nextState.get('players').get(0).get('hoplites').get(1)).to.equal(Map({x: 2, y: 1, hasMoved: true}));
  });
  
  it('hoplites can not move to none adjacent squares', () => {
    const state = fromJS({
        map: TEST_MAP,
        players:
        [
            {
                hoplites: [{x: 0, y: 0}]
            }
        ],
        phase: PHASES.MOVE_HOPLITES_1,
        currentPlayer: 0
      });
    const nextState = moveHoplite(state, {playerIndex: 0, hopliteIndex: 0, target: {x: 0, y: 2}});
    expect(nextState.get('players').get(0).get('hoplites').get(0)).to.equal(Map({x: 0, y: 0}));
  });
  
  it('hoplites cannot move out of turn', () => {
    const state = fromJS({
        map: TEST_MAP,
        players:
        [
            {
                hoplites: [{x: 0, y: 0}]
            },
             {
                hoplites: [{x: 1, y: 1}]
            }
        ],
        phase: PHASES.MOVE_HOPLITES_2,
        currentPlayer: 1
      });
    let nextState = moveHoplite(state, {playerIndex: 0, hopliteIndex: 0, target: {x: 1, y: 0}});    
    expect(nextState.get('players').get(0).get('hoplites').get(0)).to.equal(Map({x: 0, y: 0}));
  });

  it('hoplites cannot move twice', () => {
    const state = fromJS({
        map: TEST_MAP,
        players:
        [
            {
                hoplites: [{x: 2, y: 2}]
            }
        ],
        phase: PHASES.MOVE_HOPLITES_1,
        currentPlayer: 0
      });
    let nextState = moveHoplite(state, {playerIndex: 0, hopliteIndex: 0, target: {x: 2, y: 1}});
    nextState = moveHoplite(nextState, {playerIndex: 0, hopliteIndex: 0, target: {x: 1, y: 1}});
    expect(nextState.get('players').get(0).get('hoplites').get(0)).to.equal(Map({x: 2, y: 1, hasMoved: true}));
  });
});
