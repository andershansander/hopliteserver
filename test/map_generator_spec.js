import {List, Map} from 'immutable';
import {expect} from 'chai';

import {generateMap} from '../src/game_logic/map_generator.js';
import _ from "underscore";

describe('map generation logic -', () => {

    it('adds a map to the state', () => {
        const state = Map();
        const nextState = generateMap(state, {numPlayers: 4});
        expect(nextState.get('map')).to.not.be.null;
    });
    
    it('throws exception if number of players is less than 2', () => {
        const state = Map();
        const generate1PlayerMap = function () {generateMap(state, {numPlayers: 1})};
        expect(generate1PlayerMap).to.throw('Bad number of players. Can not generate map for 1 players');
    });
    it('throws exception if number of players is more than 4', () => {
        const state = Map();
        const generate1PlayerMap = function () {generateMap(state, {numPlayers: 5})};
        expect(generate1PlayerMap).to.throw('Bad number of players. Can not generate map for 5 players');
    });
    it('two players generates a 7*7 map', () => {
        const state = Map();
        const nextState = generateMap(state, {numPlayers: 2});
        expect(nextState.get('map').get('0').size).to.equal(6);
        expect(nextState.get('map').size).to.equal(7);
    });
    it('three players generates a 8*9 map', () => {
        const state = Map();
        const nextState = generateMap(state, {numPlayers: 3});
        expect(nextState.get('map').get('0').size).to.equal(7);
        expect(nextState.get('map').size).to.equal(9);
    });
    it('four players generates a 10*9 map', () => {
        const state = Map();
        const nextState = generateMap(state, {numPlayers: 4});
        expect(nextState.get('map').get('0').size).to.equal(9);
        expect(nextState.get('map').size).to.equal(9);
    });
    
    it('four players generates a map with expected number of each landscape type', () => {
        const state = Map();
        const nextState = generateMap(state, {numPlayers: 4});
        const flatMap = _.flatten(nextState.get('map').toJS());
        var numDesert = flatMap.filter(element => element === 'D').length;        
        var numPlains = flatMap.filter(element => element === 'P').length;
        var numForrest = flatMap.filter(element => element === 'F').length;
        var numMountains = flatMap.filter(element => element === 'M').length;
        var numRivers = flatMap.filter(element => element === 'R').length;
        var numVillages = flatMap.filter(element => element === 'V').length;
        
        expect(numPlains + numDesert + numForrest + numMountains + numRivers + numVillages).to.equal(77);
        
        expect(numDesert).to.be.above(17);
        expect(numDesert).to.be.below(26);
        
        expect(numPlains).to.be.above(9);
        expect(numPlains).to.be.below(18);
        
        expect(numRivers).to.be.above(9);
        expect(numRivers).to.be.below(18);
        
        expect(numMountains).to.be.above(5);
        expect(numMountains).to.be.below(14);
        
        expect(numVillages).to.equal(2);
        
        expect(numForrest).to.be.above(3);
        expect(numForrest).to.be.below(12);
    });
    
});