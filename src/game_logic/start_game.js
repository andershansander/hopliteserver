import {List, Map, fromJS, toJS} from 'immutable';
import {generateMap} from './map_generator.js';
import PHASES from './phases.js';
import {generateDeck} from './utilities/deck.js';

export function startGame(state, {players, gameId}) {
    if (!state) {
        state = Map();
    }
    if (!Map.isMap(state)) {
        throw "The state parameter to startGame should be an immutable map.";
    }
    let playerList = List();
    players.forEach((player) => {
        playerList = playerList.push(
            fromJS({
                name: player,
                cities: List(),
                hoplites: List(),
                settlers: List(),
                cards: List(),
                food: 10,
                gold: 10,
                industry: 0
            })
        );
    });
    state = generateMap(state, {numPlayers: players.length});
    state = state.set('players', playerList);
    state = state.set('phase', 1);
    state = state.set('currentPlayer', 0);
    state = state.set('phases', Map(PHASES));
    state = state.set('id', gameId);
    state = state.setIn(['secret', 'deck'], generateDeck());

    let player = 0;
    const startPositions = state.get('map').forEach((row, y) => {
        const startPositionsOnRow = row.map((hex, x) => {return {type: hex, x: x, y: y}}).filter(hex => hex.type === 'S');
        startPositionsOnRow.forEach(hex => {
            state = state.setIn(['players', player, 'cities'], fromJS([{x: hex.x, y: hex.y, hasCaravan: false}]));
            state = state.setIn(['players', player, 'settlers'], fromJS([{x: hex.x, y: hex.y}]));
            player++;
        });
    });

    return state;
}