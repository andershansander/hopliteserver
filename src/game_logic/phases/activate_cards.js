import PHASE from '../phases.js';
import {endTurn} from './shared/end_turn.js';
import {List} from 'immutable';

function isCorrectPhase(state) {
    return state.get('phase') === PHASE.ACTIVATE_CARDS;
}

export function expectUserInput(state) {
    const cards = state.get('players').get(state.get('currentPlayer')).get('cards');
    return cards && cards.size > 0; 
}

export function endPhase(state) {
    return endTurn(state);
}

export function playCard(state, {playerIndex, cardIndex}) {
    if (!isCorrectPhase(state)) {
        console.log("Player " + playerIndex + " is trying to play a card in phase " + state.get('phase'));
        return state;
    }
    let nextState = state;    
    const player = state.get('players').get(playerIndex);
    if (!player || !player.get('cards') || cardIndex < 0 || cardIndex >= player.get('cards').size) {
        console.log("Player " + playerIndex + " does not have a card with index " + cardIndex);
    } else {
        let card = player.get('cards').get(cardIndex);        
        card = card.set('round', 1);
        card = card.set('playedByPlayerIndex', playerIndex);        
        nextState = nextState.setIn(['players', playerIndex, 'cards'], player.get('cards').remove(cardIndex));
        const activeCards = nextState.get('activeCards') || List();
        nextState = nextState.set('activeCards', activeCards.push(card));
    }
    return nextState;
}
