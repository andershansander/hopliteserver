import {endTurn} from './shared/end_turn.js';

export function endPhase(state) {
    return endTurn(state);
}

export function expectUserInput(state) {
    return false;
}

export function updateCardCounters(state) {
    let nextState = state;
    const activeCards = nextState.get('activeCards');
    if (activeCards) {
        const expiringCards = activeCards.filter(card => card.get('lifespan') <= card.get('round'));
        const liveCards = activeCards.filter(card => card.get('lifespan') > card.get('round'));
        if (expiringCards.size + liveCards.size !== activeCards.size) {
            console.log("ERROR - cards with invalid counters found");
            console.log("Active cards are " + JSON.stringify(activeCards));
        }
        nextState = nextState.set('activeCards', liveCards.map(card => card.set('round', card.get('round') + 1)));
        nextState = nextState.setIn(['secret', 'discardedCards'], expiringCards.map(card => card.filter((value, index) => index === 'id' || index === 'lifespan')));
    }
    return nextState;
}

export function onEnterPhase(state) {
    return updateCardCounters(state);
}