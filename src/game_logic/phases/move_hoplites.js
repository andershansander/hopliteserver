import {moveUnit} from './shared/move_units.js';

export function moveHoplite (state, {hopliteIndex, playerIndex, target}) {
    return moveUnit(state, {target: target, unitIndex: hopliteIndex, playerIndex: playerIndex, unitType: 'hoplites'});
}


export function endPhase (state) {
    let nextState = state;
    let hoplites = nextState.get('players').get(nextState.get('currentPlayer')).get('hoplites');
    if (!hoplites) {
        hoplites = List();
    }
    hoplites = hoplites.map(hoplite => {
        return hoplite.remove('hasMoved');
    });
    nextState = nextState.setIn(["players", nextState.get('currentPlayer'), 'hoplites'], hoplites);
    nextState = performBattles(nextState);
    nextState = nextState.set('solvis', 0);
    nextState = nextState.set('phase', nextState.get('phase') + 1);
    return nextState;
}

import {List, Map, fromJS} from 'immutable';

function toPositionKey(x, y) {
    return x + "," + y;
}
function fromPositionKey(positionKey) {
    return {
        x: Number(positionKey.split(',')[0]),
        y: Number(positionKey.split(',')[1])
    };
}

function addUnitsToConflictPosition ({unitType, conflictPositions, player, playerIndex}) {
    player.get(unitType).forEach((unit, unitIndex) => {
        const positionKey = toPositionKey(unit.get('x'), unit.get('y'));        
        conflictPositions[positionKey] = conflictPositions[positionKey] || {};        
        conflictPositions[positionKey][playerIndex] = conflictPositions[positionKey][playerIndex] || {hoplites: [], cities: [], settlers: []};
        conflictPositions[positionKey][playerIndex][unitType].push(unitIndex);
    });
} 

/**
 * Creates a structure like:
 * {
 *      "x,y": {
 *          "playerIndexX": {hoplites: [hopliteIndexZ]: cities: [cityIndexX]},
 *          "playerIndexY": {hoplites: [hopliteIndexZ2]}
 * }}
 */
function getConflictPositions (state) {
    let conflictPositions = {};
    state.get('players').forEach((player, playerIndex) => {
        ['hoplites', 'settlers', 'cities'].forEach(unitType => {
            if (player.get(unitType)) {
                addUnitsToConflictPosition({
                    unitType: unitType, 
                    conflictPositions: conflictPositions,
                    player: player,
                    playerIndex: playerIndex});
            }
        });
    });
    //only keep positions with hoplites and entities from more than one player
    const realConflictKeys = Object.keys(conflictPositions).filter(key => Object.keys(conflictPositions[key]).length > 1 && Object.keys(conflictPositions[key]).some(playerIndex => conflictPositions[key][playerIndex].hoplites.length > 0));
    const result = {};
    realConflictKeys.forEach(key => result[key] = conflictPositions[key]);
    return result;
}

function transferCity(state, location, fromPlayerIndex, toPlayerIndex) {
    let nextState = state;
    const invadedPlayerCities = nextState.get('players').get(fromPlayerIndex).get('cities');
    const invadingPlayerCities = nextState.get('players').get(toPlayerIndex).get('cities');
    nextState = nextState.setIn(['players', fromPlayerIndex, 'cities'], invadedPlayerCities.filter(city => city.get('x') !== location.x || city.get('y') !== location.y));
    return nextState.setIn(['players', toPlayerIndex, 'cities'], invadingPlayerCities.push(fromJS({x: location.x, y: location.y})));
}

function performBattle(state, conflictPosition, location) {
    console.log("PerformBattle ", Object.keys(conflictPosition))
    let nextState = state;
    const cityOwnerAtLocation = Object.keys(conflictPosition).find(playerIndex => conflictPosition[playerIndex].cities && conflictPosition[playerIndex].cities.length > 0);
    let largestForce = 0;
    let secondLargestForce = 0;
    let winningPlayerIndex = null;
    Object.keys(conflictPosition).forEach(playerIndex => {
        const bonus = cityOwnerAtLocation === playerIndex ? 2 : 0;
        const numHoplites = conflictPosition[playerIndex].hoplites ? conflictPosition[playerIndex].hoplites.length : 0; 
        const playerForce =  numHoplites + bonus;
        if (playerForce > largestForce) {
            winningPlayerIndex = playerIndex;
            secondLargestForce = largestForce;
            largestForce = playerForce;
        } else if (playerForce === largestForce) {
            winningPlayerIndex = null;
            secondLargestForce = playerForce;
        } else if (playerForce > secondLargestForce) {
            secondLargestForce = playerForce;
        }
    });
    for (let playerIndex in Object.keys(conflictPosition)) {
        if (winningPlayerIndex === playerIndex) {
            const bonus = playerIndex === cityOwnerAtLocation ? 2 : 0;
            if (secondLargestForce > bonus) {
                const hoplitesToRemove = nextState.get('players').get(playerIndex).get('hoplitesToRemove') || List();            
                nextState = nextState.setIn(['players', playerIndex, 'hoplitesToRemove'], hoplitesToRemove.push(fromJS(
                    {
                        location: location,
                        amount: secondLargestForce - bonus
                    }
                )));
            }            
            if (cityOwnerAtLocation && cityOwnerAtLocation !== playerIndex) {
                nextState = transferCity(nextState, location, cityOwnerAtLocation, playerIndex);
            }
        } else {       
            const playerHoplites = nextState.get('players').get(playerIndex).get('hoplites');
            const playerSettlers = nextState.get('players').get(playerIndex).get('settlers');
            if (playerHoplites) {
                nextState = nextState.setIn(['players', playerIndex, 'hoplites'], playerHoplites.filter(hoplite => {
                    return hoplite.get('x') !== location.x || hoplite.get('y') !== location.y;
                }));
            }
            if (playerSettlers) {
                nextState = nextState.setIn(['players', playerIndex, 'settlers'], playerSettlers.filter(settler => settler.get('x') !== location.x || settler.get('y') !== location.y));
            }
        }
    }
    return nextState;
}


export function expectUserInput(state) {
    const hoplites = state.get('players').get(state.get('currentPlayer')).get('hoplites');
    return hoplites && hoplites.size > 0;
}


export function performBattles (state) {
    let nextState = state;
    const conflictPositions = getConflictPositions(nextState);
    Object.keys(conflictPositions).forEach(positionKey => {
        nextState = performBattle(nextState, conflictPositions[positionKey], fromPositionKey(positionKey));        
    });

    return nextState;
}