import {setIndustryOfPlayers} from './income.js';

export function removeHoplites(state, {hopliteIndices}) {
    let nextState = state;
    const playerIndex = state.get('solvis');
    let hoplitesToRemove = nextState.get('players').get(playerIndex).get('hoplitesToRemove');
    let hoplites = nextState.get('players').get(playerIndex).get('hoplites');
    
    const toActuallyRemove = [];

    hopliteIndices.forEach(hopliteIndex => {
        const hoplite = hoplites.get(hopliteIndex);
        if (!hoplite) {
            console.log("Player " + playerIndex + " has no hoplite with hoplite index " + hopliteIndex + " to remove.");
        } else {
            const removeListIndex = hoplitesToRemove.findIndex(toRemove => toRemove.get('location').get('x') === hoplite.get('x') && toRemove.get('location').get('y') === hoplite.get('y'));
            if (removeListIndex > -1) {
                toActuallyRemove.push(hopliteIndex);
                const hopliteToRemove = hoplitesToRemove.get(removeListIndex);
                hoplitesToRemove = hoplitesToRemove.set(removeListIndex, hopliteToRemove.set('amount', hopliteToRemove.get('amount') - 1));
            } else {
                console.log("Player " + playerIndex + " is trying to remove a hoplite from a position where there is no remove necessary.");
            }
        }
    });

    hoplites = hoplites.filter((hoplite, hopliteIndex) => toActuallyRemove.every(hopliteToRemoveIndex => hopliteIndex !== hopliteToRemoveIndex));    
    nextState = nextState.setIn(["players", playerIndex, "hoplites"], hoplites);
    nextState = nextState.setIn(["players", playerIndex, "hoplitesToRemove"], hoplitesToRemove.filter(toRemove => toRemove.get('amount') > 0));
    return nextState;
}


export function expectUserInput(state) {
    const hoplitesToRemove = state.get('players').get(state.get('solvis')).get('hoplitesToRemove');
    return hoplitesToRemove && hoplitesToRemove.size > 0;
}

export function allConflictsSolved(state) {
    const hoplitesToRemove = state.get('players').get(state.get('solvis')).get('hoplitesToRemove');
    return !(hoplitesToRemove && hoplitesToRemove.find(toRemove => toRemove.get('amount') > 0));
}

export function endPhase(state) {
    let nextState = state;
    if (nextState.get('solvis') === nextState.get('players').size - 1) {
        nextState = nextState.remove('solvis');
        if (nextState.get('currentPlayer') === nextState.get('players').size - 1) {
            nextState = nextState.set('currentPlayer', 0);
            nextState = nextState.set('phase', nextState.get('phase') + 1);
        } else {
           nextState = nextState.set('phase', nextState.get('phase') - 1);
           nextState = nextState.set('currentPlayer', nextState.get('currentPlayer') + 1);
        }
    } else {
        nextState = nextState.set('solvis', nextState.get('solvis') + 1);
    }
    nextState = setIndustryOfPlayers(nextState);
    return nextState;
}

