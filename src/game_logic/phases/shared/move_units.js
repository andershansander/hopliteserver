import {List, Map} from 'immutable';
import {adjacentSquare} from '../../utilities/map.js';
import PHASES from '../../phases.js';

const singular = {
    "hoplites": "hoplite",
    "settlers": "settler"   
};

export function moveUnit(state, {target, unitType, unitIndex}) {
    let nextState = state;
    const playerIndex = state.get('currentPlayer');
    if ((unitType === 'settlers' && state.get('phase') !== PHASES.MOVE_SETTLERS) || (unitType === 'hoplites' && state.get('phase') !== PHASES.MOVE_HOPLITES_1 && state.get('phase') !== PHASES.MOVE_HOPLITES_2 )) {
        console.log("Player " + playerIndex + " is trying to move a " + singular[unitType] + " in phase " + state.get('phase'));
        return nextState;
    }
    const player = state.get('players').get(playerIndex);
    
    const unit = player.get(unitType).get(unitIndex);
    if (!unit) {
        throw "Player " + playerIndex + " is trying to move a none existant " + singular[unitType];
    }
    if (unit.get('hasMoved')) {
        console.log("Player " + playerIndex + " is trying to move a " + singular[unitType] + " that has already moved.");
        return nextState;
    }
    if (!state.get('map').get(target.y) || !state.get('map').get(target.y).get(target.x)) {
        console.log("The target " + target.x + ", " + target.y + " is outside the map and cannot be reached by a  " + singular[unitType]);
        return nextState;
    }
    const mapSquare = state.get('map').get(target.y).get(target.x);    
    if (!(['P', 'M', 'F', 'R', 'D', 'V', 'S'].some(validType => validType === mapSquare))) {
        console.log("The hex at " + target.x + ", " + target.y + " is not a square which a " + singular[unitType] + " can move to. It is of type " + mapSquare);
        return nextState;
    }
    const summedCoordinateDistance = Math.abs(target.x - unit.get('x')) + Math.abs(target.y - unit.get('y'));
    if (summedCoordinateDistance === 0) {
        console.log("Player " + playerIndex + " is moving a " + singular[unitType] + " to it's current position. Ignoring action.");
        return nextState;
    } 

    if (adjacentSquare({x: unit.get('x'), y: unit.get('y')}, target)) {
        return nextState.setIn(["players", playerIndex, unitType, unitIndex], unit.set('hasMoved', true).set('x', target.x).set('y', target.y));
    } else {
        console.log("The hex at " + target.x + ", " + target.y + " is not reachable for the " + singular[unitType] + " at " + unit.get('x') + ", " + unit.get('y'));
        return nextState;
    }
}
