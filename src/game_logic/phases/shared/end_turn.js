import PHASES from '../../phases.js';

export function endTurn(state) {
    let nextState = state;    
    const phase = nextState.get('phase');
    if (nextState.get('currentPlayer') === nextState.get('players').size - 1) {
        nextState = nextState.set('currentPlayer', 0);
        nextState = nextState.set('phase', phase === PHASES.PAY_MERCENARIES ? PHASES.MOVE_SETTLERS : phase + 1);
    } else {
        nextState = nextState.set('currentPlayer', nextState.get('currentPlayer') + 1);
    }
    return nextState;
}