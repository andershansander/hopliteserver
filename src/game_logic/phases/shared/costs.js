export function getSettlerCost(state) {
    return {food: 4, gold: 2};
}

export function getHopliteCost(state) {
    return {food: 1, gold: 2, industry: 1};
}

export function getCaravanCost(state) {
    return {food: 2, gold: 2};
}