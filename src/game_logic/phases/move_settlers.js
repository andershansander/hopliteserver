import {List, Map} from 'immutable';
import {anyoneHasCityAt} from '../utilities/player.js';
import PHASES from '../phases.js';
import {moveUnit} from './shared/move_units.js';
import {setIndustryOfPlayers} from './income.js';
import {endTurn} from './shared/end_turn.js';

export function buildCity (state, {settlerIndex}) {
    let nextState = state;
    const playerIndex = state.get('currentPlayer');
    if (nextState.get('players').get(playerIndex).get('cities') === undefined) {
        nextState = nextState.setIn(["players", playerIndex, 'cities'], List());
    }
    if (nextState.get('players').get(playerIndex).get('settlers') === undefined) {
        nextState = nextState.setIn(["players", playerIndex, 'settlers'], List());
    }
    if (state.get('phase') !== PHASES.MOVE_SETTLERS) {
        console.log("Player " + playerIndex + " is trying to build a city in phase " + state.get('phase'));
        return nextState;
    }
    const player = state.get('players').get(playerIndex);
    if (settlerIndex < 0 || settlerIndex >= player.get('settlers').size) {
        console.log("Player " + playerIndex + " does not have a settler with settlerIndex " + settlerIndex);
        return nextState;
    }
    const foodCost = player.get('cities').size;
    if (player.get('food') < foodCost) {
        console.log("Player " + playerIndex + " cannot afford to build a city.");
        return nextState;
    }
    const settler = player.get('settlers').get(settlerIndex);    
    if (anyoneHasCityAt(state.get('players'), {x: settler.get('x'), y: settler.get('y')})) {
        console.log("The hex at " + settler.get('x') + ", " + settler.get('y') + " is occupied");
        return nextState;
    }
    if (settler.get('hasMoved')) {
        console.log("The selected settler has already moved and cannot build a city.");
        return nextState;
    }
    const mapSquare = state.get('map').get(settler.get('y')).get(settler.get('x'));
    if (!(['P', 'M', 'F', 'R', 'D'].some(validType => validType === mapSquare))) {
        console.log("The hex at " + settler.get('x') + ", " + settler.get('y') + " is not a buildable. It is of type " + mapSquare);
        return nextState;
    }
    const cities = nextState.get('players').get(playerIndex).get('cities');
    const settlers = nextState.get('players').get(playerIndex).get('settlers');
    nextState = nextState.setIn(["players", playerIndex, 'food'], player.get('food') - foodCost);
    nextState = nextState.setIn(["players", playerIndex, 'cities'], cities.push(Map({x: settler.get('x'), y: settler.get('y'), hasCaravan: false})));
    nextState = nextState.setIn(["players", playerIndex, 'settlers'], settlers.remove(settlerIndex));
    return nextState;
};

export function moveSettler (state, {settlerIndex, playerIndex, target}) {
    return moveUnit(state, {target: target, unitIndex: settlerIndex, playerIndex: playerIndex, unitType: 'settlers'});
}


export function expectUserInput(state) {
    const settlers = state.get('players').get(state.get('currentPlayer')).get('settlers');
    return settlers && settlers.size > 0;
}

export function endPhase (state) {
    let settlers = state.get('players').get(state.get('currentPlayer')).get('settlers');
    if (!settlers) {
        settlers = List();
    }
    settlers = settlers.map(settler => {
        return settler.remove('hasMoved');
    });
    state = setIndustryOfPlayers(state);
    state = state.setIn(["players", state.get('currentPlayer'), 'settlers'], settlers); 
    return endTurn(state);
}