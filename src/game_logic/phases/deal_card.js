import {Map, List, fromJS} from 'immutable';
import {endTurn} from './shared/end_turn.js';
import {shuffleCards} from '../utilities/deck.js';

function setFirstBidder(state) {
    if (!hasEnoughMoney(state, state.get('players').get(state.get('currentPlayer')))) {
        return updateCurrentPlayer(state);
    } else {
        return state;    
    }    
}

export function onEnterPhase(state) {
    if (!hasEveryoneBidded(state)) {
        return showTopCard(setFirstBidder(state));
    }
    return state;
}

function showTopCard(state) {
    let nextState = state;
    if (!nextState.getIn(['secret', 'deck'])) {
        console.log("No deck. Cannot show top card");
        return nextState;
    }
    const deck = nextState.getIn(['secret', 'deck']);
    const card = deck.first();    
    nextState = nextState.setIn(['secret', 'deck'], deck.shift());
    nextState = nextState.set('shownCard', card);
    return nextState;
}

function hasEnoughMoney(state, player) {
    const bid = state.get('cardBid');
    const neededMoney =  bid ? bid.get('gold') + 1: 3;
    return player.get('gold') && player.get('gold') >= neededMoney;
}

function hasEveryoneBidded(state) {
    const playerStillToAct = state.get('players').some((player, playerIndex) => {        
        return hasEnoughMoney(state, player) && !playerIsOutOfBidding(state, playerIndex) && !playerHasHighestBid(state, playerIndex); 
    });
    return !playerStillToAct;
}

function playerIsOutOfBidding(state, playerIndex) {
    const outOfBidding = state.get('outOfBidding') || List();
    return outOfBidding.some(passedPlayerIndex => passedPlayerIndex === playerIndex);
}
function playerHasHighestBid(state, playerIndex) {
    const bid = state.get('cardBid');    
    if (!bid) {
        return false;
    }
    return bid.get('playerIndex') === playerIndex;
}

function updateCurrentPlayer(state) {
    let nextState = state;
    nextState = nextState.set('currentPlayer', nextState.get('currentPlayer') === nextState.get('players').size - 1 ? 0 : nextState.get('currentPlayer') + 1);
    while(!hasEnoughMoney(nextState, nextState.get('players').get(nextState.get('currentPlayer')))) {
        nextState = nextState.set('currentPlayer', nextState.get('currentPlayer') === nextState.get('players').size - 1 ? 0 : nextState.get('currentPlayer') + 1);
    }
    return nextState;
}
export function bidOnShownCard(state, {gold}) {
    let nextState = state;
    const playerIndex = state.get('currentPlayer');
    const bid = new Map({'playerIndex': playerIndex, 'gold': gold});
    nextState = nextState.set('cardBid', bid);
    if (hasEveryoneBidded(nextState)) {
        nextState = completeCardBidding(nextState);
    } else {
        nextState = updateCurrentPlayer(nextState);
    }
    return nextState;
}

export function passOnShownCard(state) {
    let nextState = state;
    const playerIndex = state.get('currentPlayer');
    const outOfBidding = nextState.get('outOfBidding') || List();
    nextState = nextState.set('outOfBidding', outOfBidding.push(playerIndex));
    if (hasEveryoneBidded(nextState)) {
        nextState = completeCardBidding(nextState);
    } else {
        nextState = updateCurrentPlayer(nextState); 
    }
    return nextState;
}

export function expectUserInput(state) {
    return !hasEveryoneBidded(state) && !state.get('biddingCompleted');
}

function completeCardBidding(state) {
    let nextState = state;
    nextState = nextState.remove('outOfBidding');
    const bid = nextState.get('cardBid');
    if (bid) {
        const playerGold = nextState.getIn(['players', bid.get('playerIndex'), 'gold']);
        nextState = nextState.setIn(['players', bid.get('playerIndex'), 'cards'], nextState.get('shownCard'));
        nextState = nextState.setIn(['players', bid.get('playerIndex'), 'gold'], playerGold - bid.get('gold'));
        nextState = nextState.remove('shownCard');
        nextState = nextState.remove('cardBid');        
    } else if (nextState.get('shownCard')) {        
        const discardedCards = nextState.getIn(['secret', 'discardedCards'], new List());    
        nextState = nextState.setIn(['secret', 'discardedCards'], discardedCards.push(nextState.get('shownCard')));        
        nextState = nextState.remove('shownCard');
    }
    let deck = nextState.getIn(['secret', 'deck'], List());
    const discardedCards = nextState.getIn(['secret', 'discardedCards'], new List());
    if (deck.size === 0 && discardedCards.size > 0) {
        nextState = nextState.setIn(['secret', 'deck'], fromJS(shuffleCards(discardedCards.toJS())));
        nextState = nextState.setIn(['secret', 'discardedCards'], new List());
    }
    return nextState.set('biddingCompleted', true);
}


export function endPhase(state) {
    if (!expectUserInput(state)) {
        return endTurn(state.remove('biddingCompleted'));
    } else {
        console.log("Bidding not completed. Cannot end turn.");
        return state;
    }
    
}

