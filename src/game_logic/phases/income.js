import {List, Map} from 'immutable';
import PHASES from '../phases.js';
import {distance} from '../utilities/map.js';
import {endTurn} from './shared/end_turn.js';

const TILE_INCOME = {
    'P': {
        food: 1,
        gold: 0,
        industry: 0
    },
    'D': {
        food: 0,
        gold: 0,
        industry: 0
    },
    'R': {
        food: 0,
        gold: 2,
        industry: 0
    },
    'M': {
        food: 0,
        gold: 0,
        industry: 2
    },
    'F': {
        food: 0,
        gold: 0,
        industry: 3
    },
    'S': {
        food: 1,
        gold: 1,
        industry: 0
    }
};


export function endPhase(state) {
    return endTurn(collectIncome(state));
}

export function expectUserInput(state) {
    return false;
}

export function setIndustryOfPlayers(state) {
    const players = state.get('players');
    players.forEach((player, playerIndex) => {
        if (!player.get('cities')) {
            player = player.set('industry', 0);
        } else {
            const industry = player.get('cities').reduce((totalIndustry, city) => {
                const tileType = state.get('map').get(city.get('y')).get(city.get('x'));
                return totalIndustry + TILE_INCOME[tileType].industry;
            }, 0);
            player = player.set('industry', industry);
        }
        state = state.setIn(['players', playerIndex], player)
    });
    return state;
}


function calculateCaravanIncome(cities, villages) {
    let bestOptionIncome = 0;
    cities.forEach((city, cityIndex) => {
        let optionIncome = 0;
        const options = villages.filter(village => distance({x: city.get('x'), y: city.get('y')}, {x: village.get('x'), y: village.get('y')}) < 3);
        options.forEach(option => {
            optionIncome = 1 + calculateCaravanIncome(cities.remove(cityIndex), villages.remove(option.get('index')));
            if (optionIncome > bestOptionIncome) {
                bestOptionIncome = optionIncome;
            }
        });
    });
    return bestOptionIncome;
}

function getCaravanIncome(state) {
    let player = state.get('players').get(state.get('currentPlayer'));
    if (!player.get('cities') || player.get('cities').size === 0) {
        return state;
    } 
    let neutralVillages = List();
    let villageIndex = 0;
    state.get('map').forEach((row, rowIndex) => {
        row.forEach((column, columnIndex) => {
            if (column === 'V') {
                neutralVillages = neutralVillages.push(Map({x: columnIndex, y: rowIndex, index: villageIndex}));
                villageIndex++;
            }
        });
    });
    neutralVillages = neutralVillages.filter(village => {
        return !state.get('players').some((player, playerIndex) => {
            return playerIndex !== state.get('currentPlayer') && player.get('hoplites') && player.get('hoplites').some(hoplite => hoplite.get('x') === village.get('x') && hoplite.get('y') === village.get('y'));
        });
    });
    const income = calculateCaravanIncome(player.get('cities').filter(city => city.get('hasCaravan')), neutralVillages);
    const currentGold = player.get('gold') || 0;
    return state.setIn(['players', state.get('currentPlayer'), 'gold'], currentGold + income);
}

export function collectIncome(state) {
    if (state.get('phase') !== PHASES.INCOME) {
        console.log("Collect income called in wrong phase. Current phase is " + state.phase);
        return state;
    }

    let player = state.get('players').get(state.get('currentPlayer'));
    let income;
    if (!player.get('cities')) {
        income = {
            food: 0,
            gold: 0,
            industry: 0
        }
    } else {
        income = player.get('cities').reduce((income, city) => {
            const tileType = state.get('map').get(city.get('y')).get(city.get('x'));
            return {
                food: income.food + TILE_INCOME[tileType].food,
                gold: income.gold + TILE_INCOME[tileType].gold,
                industry: income.industry + TILE_INCOME[tileType].industry,
            };
        }, {food: 0, gold: 0, industry: 0});
    }    

    const currentGold = player.get('gold') || 0;
    const currentFood = player.get('food') || 0;

    player = player.set('gold', currentGold + income.gold);
    player = player.set('food', currentFood + income.food);
    player = player.set('industry', income.industry);

    return getCaravanIncome(state.setIn(['players', state.get('currentPlayer')], player));
}