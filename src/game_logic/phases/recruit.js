import {List, Map} from 'immutable';
import {getCityAt, hasCityAt, getCityIndexAt} from '../utilities/player.js';
import PHASES from '../phases.js';
import {endTurn} from './shared/end_turn.js';

function canAfford(player, cost, unitName) {
    let result = true;
    if (cost.gold && player.get('gold') < cost.gold) {
        console.log("Player " + player + " have to little gold to recruit " + unitName);
        result = false;
    }
    if (cost.food && player.get('food') < cost.food) {
        console.log("Player " + player + " have to little food to recruit " + unitName);
        result = false;
    }
    const playerRegularHoplites = player.get('hoplites').filter(hoplite => !hoplite.get('isMercenary'));        
    if (cost.industry && !(player.get('industry') > playerRegularHoplites.size)) {
        console.log("Player " + player + " have to little industry to recruit " + unitName);
        result = false;
    }

    return result;
}

function pay(player, cost) {
    let nextPlayerState = player;
    if (cost.gold) {
        nextPlayerState = nextPlayerState.set('gold', player.get('gold') - cost.gold);
    }
    if (cost.food) {
        nextPlayerState = nextPlayerState.set('food', player.get('food') - cost.food);
    }
    return nextPlayerState;
}

export function recruitSettler (state, {target, cost}) {
    let nextState = state;
    const playerIndex = state.get('currentPlayer');
    let player = state.get('players').get(playerIndex);
    if (player.get('settlers') === undefined) {
        player = player.set('settlers', List());
        nextState = nextState.setIn(["players", playerIndex], player);
    }
    if (player.get('hoplites') === undefined) {
        player = player.set('hoplites', List());
        nextState = nextState.setIn(["players", playerIndex], player);
    }
    if (state.get('phase') !== PHASES.RECRUIT) {
        console.log("Player " + playerIndex + " is trying to recruit settler in phase " + state.get('phase'));
        return nextState;
    }
    if (!canAfford(player, cost, "settler")) {
        return nextState;
    }
    if (!hasCityAt(player, target)) {
        console.log("Player " + playerIndex + " does not have a city at " + target.x + ", " + target.y);
        return nextState;
    }
        
    const settlers = player.get('settlers');
    player = player.set('settlers', settlers.push(Map({x: target.x, y: target.y})));
    player = pay(player, cost);
    nextState = nextState.setIn(["players", playerIndex], player);

    return nextState;
};

export function recruitHoplite (state, {target, cost}) {
    let nextState = state;
    const playerIndex = state.get('currentPlayer');
    let player = state.get('players').get(playerIndex);    
    if (player.get('hoplites') === undefined) {
        player = player.set('hoplites', List());
        nextState = nextState.setIn(["players", playerIndex], player);
    }
    if (state.get('phase') !== PHASES.RECRUIT) {
        console.log("Player " + playerIndex + " is trying to recruit hoplite in phase " + state.get('phase'));
        return nextState;
    }
    if (!canAfford(player, cost, "hoplite")) {
        return nextState;
    }
    if (!hasCityAt(player, target)) {
        console.log("Player " + playerIndex + " does not have a city at " + target.x + ", " + target.y);
        return nextState;
    }
        
    const hoplites = player.get('hoplites');
    player = player.set('hoplites', hoplites.push(Map({x: target.x, y: target.y})));
    player = pay(player, cost);
    nextState = nextState.setIn(["players", playerIndex], player);
    return nextState;
};


export function endPhase(state) {
    return endTurn(state);
}

export function expectUserInput(state) {
    return true;
}

export function recruitCaravan (state, {target, cost}) {
    let nextState = state;
    const playerIndex = state.get('currentPlayer');
    if (state.get('phase') !== PHASES.RECRUIT) {
        console.log("Player " + playerIndex + " is trying to recruit caravan in phase " + state.get('phase'));
        return nextState;
    }    
    let player = state.get('players').get(playerIndex);
    if (player.get('hoplites') === undefined) {
        player = player.set('hoplites', List());
        nextState = nextState.setIn(["players", playerIndex], player);
    }
    if (!canAfford(player, cost, "caravan")) {
        return nextState;
    }
    if (!hasCityAt(player, target)) {
        console.log("Player " + playerIndex + " does not have a city at " + target.x + ", " + target.y);
        return nextState;
    }
    if (getCityAt(player, target).get('hasCaravan')) {
        console.log("Player " + playerIndex + " already has a caravan at " + target.x + ", " + target.y);
        return nextState;
    }
        
    const cityIndex = getCityIndexAt(player, target);
    player = player.setIn(['cities', cityIndex, 'hasCaravan'], true);
    nextState = nextState.setIn(["players", playerIndex], pay(player, cost));
    return nextState;
};

