import {List} from 'immutable';
import {endTurn} from './shared/end_turn.js';

export function payMercenary (state, {hopliteIndex}) {
    let nextState = state;
    const playerIndex = state.get('currentPlayer');
    const hoplite = nextState.getIn(['players', playerIndex, 'hoplites', hopliteIndex]);
    if (!hoplite || !hoplite.get('isMercenary')) {
        console.log("Player " + playerIndex + " don't have a mercenary to pay at hoplite index " + hopliteIndex);
        return nextState;
    }
    const gold = nextState.getIn(['players', playerIndex, 'gold']);
    if (!gold || gold < 2) {
        console.log("Player " + playerIndex + " can't afford to pay mercenary.");
        return nextState;
    }
    nextState = nextState.setIn(['players', playerIndex, 'hoplites', hopliteIndex], hoplite.set('payed', true));
    nextState = nextState.setIn(['players', playerIndex, 'gold'], gold - 2);
    return nextState;
};

export function disbandMercenary (state, {hopliteIndex}) {
    let nextState = state;
    const playerIndex = state.get('currentPlayer');
    const hoplite = nextState.getIn(['players', playerIndex, 'hoplites', hopliteIndex]);
    if (!hoplite || !hoplite.get('isMercenary')) {
        console.log("Player " + playerIndex + " don't have a mercenary to disband at hoplite index " + hopliteIndex);
        return nextState;
    }
    const hoplites = nextState.getIn(['players', playerIndex, 'hoplites']);
    return nextState.setIn(['players', playerIndex, 'hoplites'], hoplites.remove(hopliteIndex));
};

export function allMercenariesManaged (state) {
    const hoplites = state.getIn(['players', state.get('currentPlayer'), 'hoplites']);
    if (!hoplites || hoplites.size === 0) {
        return true;
    }
    return hoplites.every(hoplite => !hoplite.get('isMercenary') || hoplite.get('payed'));
};

export function endPhase(state) {
    if (!allMercenariesManaged(state)) {
        console.log("Cannot end the Pay mercenary-phase when not all mercenaries have been managed.");
        return state;
    }
    let hoplites = state.get('players').get(state.get('currentPlayer')).get('hoplites');
    if (!hoplites) {
        hoplites = List();
    }
    hoplites = hoplites.map(hoplite => {
        return hoplite.remove('payed');
    });
    state = state.setIn(["players", state.get('currentPlayer'), 'hoplites'], hoplites);
    return endTurn(state);
}


export function expectUserInput(state) {
    const hoplites = state.get('players').get(state.get('currentPlayer')).get('hoplites');
    return hoplites && hoplites.some(hoplite => hoplite.get('isMercenary'));
}
