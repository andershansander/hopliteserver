
export function anyoneHasCityAt(players, target) {
    return players.some(player => hasCityAt(player, target));
}

export function hasCityAt(player, target) {
    if (!player.get('cities')) {
        return false;
    }
    return player.get('cities').some(city => {
        return city.get('x') === target.x && city.get('y') === target.y;
    });
}

export function getCityOwnerAt(players, target) {
    let owner = null;
    players.forEach((player, playerIndex) => {
        if (hasCityAt(player, target)) {
            owner = playerIndex;
        }
    });
    return owner;
}

export function getCityAt(player, target) {
    return player.get('cities').find(city => {
        return city.get('x') === target.x && city.get('y') === target.y;
    });
}

export function getCityIndexAt(player, target) {
    return player.get('cities').findIndex(city => {
        return city.get('x') === target.x && city.get('y') === target.y;
    });
}



export function anyoneHasSettlerAt(players, target) {
    return players.some(player => hasSettlerAt(player, target));
}

export function hasSettlerAt(player, target) {
    if (!player.get('settlers')) {
        return false;
    }
    return player.get('settlers').some(settler => {
        return settler.get('x') === target.x && settler.get('y') === target.y;
    });
}