import {fromJS, Map, List} from 'immutable';

const CARD_SETUP = [
    {
        id: "Call to arms",
        amount: 4
    },
    {
        id: "Camel whisperer",
        amount: 2
    },
    {
        id: "Convert barbarian",
        amount: 4
    },
    {
        id: "Fortification expert",
        amount: 2
    },
    {
        id: "General",
        amount: 2
    },
    {
        id: "Holy war",
        amount: 2
    },
    {
        id: "Mercenary",
        amount: 4
    },
    {
        id: "Mutiny",
        amount: 2
    },
    {
        id: "Nationalism",
        amount: 2
    },
    {
        id: "Pacifism",
        amount: 2
    },
    {
        id: "Propaganda",
        amount: 2
    },
    {
        id: "Siege expert",
        amount: 2
    },
    {
        id: "Siege weapon engineer",
        amount: 2
    }
];

export function generateDeck() {
    const cards = [];
    CARD_SETUP.forEach(cardType => {
        for (let i = 0; i < cardType.amount; i++) {
            cards.push({id: cardType.id});
        }
    });
    return fromJS(shuffleCards(cards));
}

function getRandom(min, max) {
  return Math.random() * (max - min) + min;
}

export function shuffleCards(cards) {
    let tempCard;
    let randomIndex1,
        randomIndex2;
    if (cards.length === 0) {
        return cards;
    }
    for(let i = 0; i < 5000; i++) {
        randomIndex1 = getRandom(0, cards.length);
        randomIndex2 = getRandom(0, cards.length);
        tempCard = cards[randomIndex1];
        cards[randomIndex1] = cards[randomIndex2];
        cards[randomIndex2] = tempCard;
    }
    return cards;
}


