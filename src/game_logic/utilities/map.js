export function adjacentSquare(location1, location2) {
    const yDiff = Math.abs(location2.y - location1.y);
    const xDiff = Math.abs(location2.x -location1.x);
    const summedCoordinateDistance = xDiff + yDiff;

    const standingOnEvenRow = location1.y % 2 === 0;
    return summedCoordinateDistance === 1 ||
        (standingOnEvenRow && summedCoordinateDistance === 2 && yDiff === 1 && location2.x - location1.x > 0) ||
        (!standingOnEvenRow && summedCoordinateDistance === 2 && yDiff === 1 && location2.x - location1.x < 1)
}

export function distance(location1, location2) {
    let currentLocation = {x: location1.x, y: location1.y};
    let steps = 0;
    while (currentLocation.x !== location2.x || currentLocation.y !== location2.y) {
        const xDirection = (location2.x - currentLocation.x) / Math.max(1, Math.abs(location2.x - currentLocation.x));
        const yDirection = (location2.y - currentLocation.y) / Math.max(1, Math.abs(location2.y - currentLocation.y));
        if (adjacentSquare(currentLocation, {x: currentLocation.x + xDirection, y: currentLocation.y + yDirection})) {
            currentLocation = {x: currentLocation.x + xDirection, y: currentLocation.y + yDirection};
            steps++;
        } else {
            currentLocation = {x: currentLocation.x, y: currentLocation.y + yDirection};
            steps++;
        }
    }
    return steps;
}