import {List, fromJS} from 'immutable';

export function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

const NUMBER_DESERTS = 25;
const NUMBER_FORREST = 11;
const NUMBER_MOUNTAINS = 13;
const NUMBER_RIVERS = 17;
const NUMBER_PLAINS = 17;


const twoPlayerMap = List([
        List(['*', '*', 'S', '*', '*', '*']),
        List(['*', '*', '*', '*', '*', '*']),
        List(['*', '*', '*', '*', '*', '*']),
        List(['V', '*', '*', '*', '*', 'V']),
        List(['*', '*', '*', '*', '*', '*']),
        List(['*', '*', '*', '*', '*', '*']),
        List(['*', '*', 'S', '*', '*', '*'])
]);
    
const threePlayerMap = List([
        List(['*', '*', '*', '*', '*', '*',  'S']),
        List(['*', '*', '*', '*', '*',  '*', '*']),
        List(['*', '*', '*', '*', '*', '*',  '*']),
        List(['*', '*', '*', 'V', '*',  '*', '*']),
        List(['S', '*', '*', '*', 'V', '*',  '*']),
        List(['*', '*', '*', '*', '*',  '*', '*']),
        List(['*', '*', '*', '*', '*', '*',  '*']),
        List(['*', '*', '*', '*', '*',  '*', '*']),
        List(['*', '*', '*', '*', '*', '*',  'S'])

]);
    
    
const fourPlayerMap = List([
        List([  '*', '*', '*', '*', 'S', '*', '*', '*', '*']),
        List(['*', '*', '*', '*', '*', '*', '*', '*', '*']),
        List([  '*', '*', '*', '*', '*', '*', '*', '*', '*']),
        List(['*', '*', '*', '*', '*', '*', '*', '*', '*']),
        List(['S', '*', '*', 'V', 'V', '*', '*', '*', 'S']),
        List(['*', '*', '*', '*', '*', '*', '*', '*', '*']),
        List([  '*', '*', '*', '*', '*', '*', '*', '*', '*']),
        List(['*', '*', '*', '*', '*', '*', '*', '*', '*']),
        List([  '*', '*', '*', '*', 'S', '*', '*', '*', '*']),
]);
    

const pieces = [];
function addToArr(arr, letter, numberOfLetter) {
    for (var i = 0; i < numberOfLetter; i++) {
        arr.push(letter);
    }
}
addToArr(pieces, 'D', NUMBER_DESERTS);
addToArr(pieces, 'F', NUMBER_FORREST);
addToArr(pieces, 'M', NUMBER_MOUNTAINS);
addToArr(pieces, 'R', NUMBER_RIVERS);
addToArr(pieces, 'P', NUMBER_PLAINS);
const allLandscapes = fromJS(pieces);

function getMapTemplate(numPlayers) {
    switch (numPlayers) {
        case 2:
            return twoPlayerMap;
        case 3:
            return threePlayerMap;
        case 4:
            return fourPlayerMap;
        default:
            throw "Bad number of players. No existing template for " + numPlayers + " players";
    }
};


export function generateMap(state, options) {
    if (!options || !options.numPlayers || options.numPlayers > 4 || options.numPlayers < 2) {
        throw ('Bad number of players. Can not generate map for ' + (options && options.numPlayers) + ' players');
    }
    
    let map = getMapTemplate(options.numPlayers);
    let landscapes = allLandscapes;
    map = map.map(row => {
       return row.map(cell => {
           if (cell === '*') {
               let randomIndex = getRandomInt(0, landscapes.size);
               let landscapeType = landscapes.get(randomIndex);
               landscapes = landscapes.delete(randomIndex);
               return landscapeType;
           } else {
               return cell;
           }
       });
    });
    
    return state.set('map', map);
}
