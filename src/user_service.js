const passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    bCrypt = require('bcrypt-nodejs');

const SALT_ROUNDS = 10;
const ONE_DAY_IN_MS = 3600 * 1000 * 24;


function isValidPassword(user, password){
  return bCrypt.compareSync(password, user.password);
}

function createToken() {
    return rand() + rand();
}

function rand() {
    return Math.random().toString(36).substr(2); // remove `0.`
};


export default function (userdbRef) {
    passport.use(new LocalStrategy(
        function(username, password, done) {
            userdbRef.findOne({ username: username }, function(err, user) {
                if (err) { return done(err); }
                if (!user) {
                    return done(null, false, { message: 'Incorrect username.' });
                }
                if (!isValidPassword(user, password)) {
                    return done(null, false, { message: 'Incorrect password.' });
                }
                if (!user.active) {
                    return done(null, false, { message: 'User is inactive.' });
                }
                return done(null, user);
            });
        }
    ));

    passport.serializeUser(function(user, done) {
        done(null, user);
    });

    passport.deserializeUser(function(user, done) {
        done(null, user);
    });

    function getUserByToken(token, callback) {
        userdbRef.findOne({ token: token }, function(err, user) {
            if (err) {
                console.log("Could not get user by token " + token + " because of the following error: " + err);
                callback(null);
            }
            if (!user || !user.active || user.tokenValidTo < Date.now()) {
                callback(null);
            }
            callback(user);
        });
    }

    function createUser(username, password) {
        try {
            bCrypt.genSaltSync(SALT_ROUNDS);
            var hash = bCrypt.hashSync(password);
            var result = userdbRef.insertOne({
                username,
                password: hash,
                created: Date.now(),
                active: true,
                lastLogin: null,
                token: null,
                tokenValidTo: null
            });
            return result.insertedId;
        } catch (e) {
            console.log("Could not create user with username " + username + " due to an exception", e);
        }
        return null;
    }


    function loginUser(username) {
        try {
            const token = createToken();
            const tokenValidTo = Date.now() + ONE_DAY_IN_MS;
            userdbRef.updateOne(
                {
                    username: username
                },
                {
                    $set: {
                        lastLogin: Date.now(),
                        token,
                        tokenValidTo
                    }
                }
            );
            return {
                token: token,
                validUntil: tokenValidTo
            };
        } catch (e) {
            console.log("User with username " + username + " could not login due to an exception", e);
        }
        return null;
    }


    return {
        validateUser: passport,
        createUser: createUser,
        loginUser: loginUser,
        getUserByToken: getUserByToken
    }
}
