var MongoClient = require('mongodb').MongoClient;

import dispatcher from './server_startup_dispatcher.js';

export default function init() {
    // create a server instance
    console.log("Opening db connection");
    MongoClient.connect('mongodb://hoplite:password@waffle.modulusmongo.net:27017/oxeZo8te',
        function(err, db) {
            if (err) {
                console.log("Could not open connection to database", err);    
            } else {
                console.log("Connected to local database");
                console.log("Connecting to user database");
                db.collection('user', function(err, userRef) {
                    if (err) {
                        console.log("Could not connect to the user table.", err);
                    } else {
                        console.log("Connected to the user database");
                        dispatcher.emit('userdb_ref_created', userRef);
                    }
                });
            }
        }
    );
}
