import {fromJS} from 'immutable';

let idCounter = 1;

export function createGame(state, {username, maxPlayers}) {
    let nextState = state;
    const pendingGames = nextState.get('pendingGames');
    nextState = nextState.set('pendingGames', pendingGames.push(fromJS({
        maxPlayers,
        players: [username],
        id: idCounter++
    })));
    return nextState;
}