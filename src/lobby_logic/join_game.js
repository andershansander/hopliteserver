import {fromJS, Map} from 'immutable';
import {startGame} from '../game_logic/start_game.js';

export function joinGame(state, {username, gameId}) {
    let nextState = state;
    const pendingGames = nextState.get('pendingGames');
    const gameIndex = pendingGames.findIndex(game => game.get('id') === gameId);
    let game = pendingGames.get(gameIndex);
    if (gameIndex < 0) {
        console.log("User with username " + username + " is trying to join a none existent game with id " + gameId);
        return nextState;
    }
    if (game.get('players').find(player => player === username)) {
        console.log("User with username " + username + " is trying to join a game which the user is already participating in");
        return nextState;
    }

    if (game.get('maxPlayers') === game.get('players').size + 1) {
        game = game.set('players', game.get('players').push(username));
        nextState = nextState.set('pendingGames', pendingGames.remove(gameIndex));
        const activeGames = nextState.get('activeGames');
        nextState = nextState.set('activeGames', activeGames.push(game));
        const gameData = startGame(Map(), {players: game.get('players').toJS(), gameId: game.get('id')});
        nextState = nextState.setIn(['gameData', game.get('id').toString()], gameData);
    }
    if (game.get('maxPlayers') > game.get('players').size) {
        game = game.set('players', game.get('players').push(username));
        nextState = nextState.setIn(['pendingGames', gameIndex], game);
    } 
    else {
        console.log("User with username " + username + " is trying to join the full game with id " + gameId);
    }    
    return nextState;
}