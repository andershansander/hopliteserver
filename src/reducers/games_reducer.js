import {Map} from 'immutable';
import gameReducer from './game_reducer.js';

function reducer(state = new Map(), action) {
    if (!action.gameId && !action.gameId === 0) {
        console.log("Got a game action without a game id.")
        return state;
    }
    let gameData = state.getIn(['gameData', action.gameId.toString()]);
    if (!gameData) {
        console.log("Game with id " + action.gameId + " not found.");
        return state; 
    }
    gameData = gameReducer(gameData, action);
    return state.setIn(['gameData', action.gameId.toString()], gameData);
}

export default reducer;