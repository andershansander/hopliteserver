import {Map} from 'immutable';
import * as recruit from '../game_logic/phases/recruit.js';
import * as moveSettlers from '../game_logic/phases/move_settlers.js';
import * as moveHoplites from '../game_logic/phases/move_hoplites.js';
import * as solveConflicts from '../game_logic/phases/solve_conflicts.js';
import * as dealCard from '../game_logic/phases/deal_card.js';
import * as cardCounters from '../game_logic/phases/card_counters.js';
import * as activateCards from '../game_logic/phases/activate_cards.js';
import * as income from '../game_logic/phases/income.js';
import * as payMercenaries from '../game_logic/phases/pay_mercenaries.js';
import PHASES from '../game_logic/phases.js';

const PHASE_LOGIC = [
  moveSettlers,
  moveHoplites,
  solveConflicts,
  moveHoplites,
  solveConflicts,
  dealCard,
  cardCounters,
  activateCards,
  recruit,
  income,
  payMercenaries
];

function endPhase(state) {
  let nextState = state;
  const lastPhase = nextState.get('phase');
  nextState = PHASE_LOGIC[nextState.get('phase') - 1].endPhase(nextState);
  if (lastPhase !== nextState.get('phase') && PHASE_LOGIC[nextState.get('phase') - 1].onEnterPhase) {
    nextState = PHASE_LOGIC[nextState.get('phase') - 1].onEnterPhase(nextState);
  }
  return nextState;
}

function continueGame(state) {
  let nextState = endPhase(state);
  while(!PHASE_LOGIC[nextState.get('phase') - 1].expectUserInput(nextState)) {
    console.log("Skipping phase " + nextState.get('phase'));
    nextState = endPhase(nextState);
  }
  return nextState;
}

function reducer(state = new Map(), action) {
  console.log("Action: ", action);
  
  const playerIndex = state.get('players').findIndex(player => action.user === player.get('name'));
  const isSolveConflictPhase = state.get('phase') === PHASES.SOLVE_CONFLICTS_1 || state.get('phase') === PHASES.SOLVE_CONFLICTS_2;
  
  const isCurrentPlayer = 
    (isSolveConflictPhase && playerIndex === state.get('solvis')) ||
    (!isSolveConflictPhase && playerIndex === state.get('currentPlayer'));
     

  if (!isCurrentPlayer) {
    console.log("Player " + playerIndex + " (" + action.user + ") is trying to act out of turn.");
    return state;
  }

  let nextState;
  switch (action.type) {
  case 'end_turn':
    return continueGame(state);
  case 'build_city':
    return moveSettlers.buildCity(state, {settlerIndex: action.settlerIndex});
  case 'move_settler':
    return moveSettlers.moveSettler(state, {settlerIndex: action.settlerIndex, target: action.target});
  case 'move_hoplite':
    return moveHoplites.moveHoplite(state, {hopliteIndex: action.hopliteIndex, target: action.target});
  case 'remove_hoplites': {
    let nextState = solveConflicts.removeHoplites(state, {hopliteIndices: action.hopliteIndices}); 
    if(solveConflicts.allConflictsSolved(nextState)) {
      nextState = continueGame(nextState);
    }
    return nextState;
  }
  case 'recruit_settler':
    return recruit.recruitSettler(state, {target: action.target, cost: {food: 4, gold: 2}});
  case 'recruit_hoplite':
    return recruit.recruitHoplite(state, {target: action.target, cost: {food: 1, gold: 2}});
  case 'recruit_caravan':
    return recruit.recruitCaravan(state, {target: action.target, cost: {food: 2, gold: 2}});
  case 'bid_on_card':
    nextState = dealCard.bidOnShownCard(state, {gold: action.gold});
    if (!dealCard.expectUserInput(nextState)) {
      return continueGame(nextState);
    } else {
      return nextState;
    }
  case 'pass_on_card':
    nextState = dealCard.passOnShownCard(state);
    if (!dealCard.expectUserInput(nextState)) {
      return continueGame(nextState);
    } else {
      return nextState;
    }
  default:
    console.log("Unknown action " + action.type);
    break;
  }
   
  return state;
}

export default reducer;