import {Map, fromJS} from 'immutable';
import gamesReducer from './games_reducer.js';
import lobbyReducer from './lobby_reducer.js';

const INITIAL_STATE = fromJS({
  pendingGames: [],  
  activeGames: [],  
  users: []
});

const reducer = function(state = INITIAL_STATE, action) {
  switch (action.category) {
  case 'lobby':
    return lobbyReducer(state, action);
  case 'auth':
    return state;
  case 'game':
    return gamesReducer(state, action);
  default:
    console.log("Unhandled action category: " + action.category); 
  }
  return state;
};

export default reducer;