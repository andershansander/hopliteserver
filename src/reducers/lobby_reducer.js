import {Map, fromJS} from 'immutable';
import {createGame} from '../lobby_logic/create_game.js';
import {joinGame} from '../lobby_logic/join_game.js';

function reducer(state = new Map(), action) {
  console.log("Lobby action: ", action);
  switch (action.type) {
  case 'create_game':
    return createGame(state, {username: action.user, maxPlayers: action.maxPlayers});
  case 'join_game':
    return joinGame(state, {username: action.user, gameId: action.gameId});
  default:
    console.log("Unknown lobby action " + action.type);
    break;
  }
   
  return state;
}

export default reducer;