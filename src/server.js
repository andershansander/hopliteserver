var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
import initUserService from './user_service.js';
import dispatcher from './server_startup_dispatcher.js';
import initDatabase from './database.js';
import playerStateFilter from './player_state_filter.js';

var cors = require('cors')

var port = process.env.PORT || 8090;
console.log("Trying to start server listening to port " + port);

const connections = {};

function startWebServer (store, userdbRef) {
  console.log("Starting hoplite server which will listen on port " + port);
  
  server.listen(port);

  const userService = initUserService(userdbRef);
  const securityFilter = userService.validateUser;
  app.use(securityFilter.initialize());  
  app.use(cors())

  app.post('/login', 
    securityFilter.authenticate('local'), 
    function (req, res) {
        console.log("User with username " + req.query.username + " is trying to login.");
        const token = userService.loginUser(req.query.username, req.query.password);
        if (token !== null) {
          console.log("User with username " + req.query.username + " just logged in.");
        }
        res.send({token: token.token, validUntil: token.validUntil, username: req.query.username});
      });
    app.post('/createUser',
      function (req, res) {
        console.log("Creating user with username " + req.query.username + " and password " + req.query.password);
        const userId = userService.createUser(req.query.username, req.query.password)
        console.log("User created with id " + userId);
        res.send();
      });

  store.subscribe(() => {
    console.log("Sending new state to " + Object.keys(connections).length + " connected sockets.");
    Object.keys(connections).forEach(connectionIndex => {
      const connection = connections[connectionIndex];      
      console.log("Sending new state to " + connection.username);
      connection.socket.emit('state', playerStateFilter(store.getState(), connection.username).toJS());
    });
  });
  
  io.on('connection', (socket) => {
    console.log("New connection with id " + socket.id + ".");
    
    connections[socket.id] = {
      validated: false,
      username: null,
      socket: socket
    };

    socket.on('disconnect', function() {
        console.log(socket.id + ' disconnected.');
        delete connections[socket.id];
    });

    socket.on('login', (action) => {
      console.log("Websocket login attempt with action " + JSON.stringify(action));
      if (!connections[socket.id].validated) {
        userService.getUserByToken(action.token, user => {
          if (user === null) {
            console.log("The connection with id " + socket.id + " is trying to login with a bad token: " + action.token);
            socket.disconnect();
          } else if (action.username === user.username) {
            connections[socket.id].username = action.username;
            connections[socket.id].validated = true;
            console.log("Websocket login attempt succeeded!");
            socket.emit('connection_validated');
            socket.emit('state', store.getState().toJS());
            console.log("Emitting state to new connection");
          } else {
            console.log("The socket with id " + socket.id + " is trying to validate with a ticket belonging to another user!");
          }
        });
      } else {
        console.log("The validated connection with id " + socket.id + " is trying to validate itself again.");
      }
    });

    socket.on('action', (action) => {
      if (connections[socket.id].validated) {
        action.user = connections[socket.id].username;
        store.dispatch(action);
      } else {
        console.log("The invalidated connection with id " + socket.id + " is trying to perform actions.");
        socket.disconnect();
      }
    });
  });
}

export default function startServer(store) {
  dispatcher.on("userdb_ref_created", startWebServer.bind(this, store))
  initDatabase();
}